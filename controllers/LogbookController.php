<?php

namespace app\controllers;

use app\models\User;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Yii;
use app\models\Logbook;
use app\models\PostLogbook;
use yii\base\DynamicModel;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Proposal;

/**
 * LogbookController implements the CRUD actions for Logbook model.
 */
class LogbookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Logbook models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PostLogbook();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $kelas = Yii::$app->user->identity->semesterMahasiswa->kelas_id;
        $kelompok = Yii::$app->user->identity->semesterMahasiswa->kelompok_id;


        if (User::isMahasiswa())
        {
            $dataProvider->query->where(['semester_mahasiswa.kelas_id' => $kelas, 'semester_mahasiswa.kelompok_id' => $kelompok]);
        }
        if (User::isKordinator())
        {
            $dataProvider->query->where(['semester_mahasiswa.kelas_id' => $kelas]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Logbook model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionExport()
    {
        $model = new DynamicModel([
            'mahasiswa_id', 'semester_kalender_id'
        ]);

        $model->addRule(['mahasiswa_id', 'semester_kalender_id'], 'safe');

        if($model->load(Yii::$app->request->post())) {
            $logbook = Logbook::find()->all();

            $data = [
                ['NIM', 'Kelompok', 'Proposal', 'Judul', 'Isi', 'Keterangan', 'Time'],
            ];

            foreach ($logbook as $logbookData) {
                $data[] = [
                    $logbookData->getMahasiswa()->one()->nim,
                    $logbookData->getSemesterMahasiswa()->one()->getKelas()->nama,
                    $logbookData->getProposal()->one()->judul,
                    $logbookData->judul,
                    $logbookData->isi,
                    $logbookData->keterangan,
                    $logbookData->time,
                ];
            }

            $spreadsheet = new Spreadsheet();
            try {
                $spreadsheet->getActiveSheet()->fromArray($data);
            } catch (Exception $e) {
                VarDumper::dump($e);
                die();
            }

            $filename = 'assets/' . md5( $this->getUniqueId() . time()) . '.xlsx';
            $writer = new Xlsx($spreadsheet);
            $writer->save($filename);

            return Yii::$app->response->sendFile($filename, 'logbook_' . $model->mahasiswa_id . '_'.$model->semester_kalender_id.'.xlsx');
        }

        return $this->render('export', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Logbook model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Logbook();

        // TODO: Retrieve data from current semester
//        $model->id_semester_mhs = $id_semester_mhs;

        if ($model->load(Yii::$app->request->post())) {
            $model->time = (new \DateTime())->format("Y-m-d H:i:s");
            if (User::isMahasiswa()) {
                $model->id_mhs = Yii::$app->user->identity->id_mahasiswa;
                $model->id_kelompok = Yii::$app->user->identity->id_kelompok;
                $model->id_kelas = Yii::$app->user->identity->id_kelas;
            }
            if($model->save(true)) {
                Yii::$app->session->setFlash('success', 'Berhasil menambahkan data');
                return $this->redirect(['view', 'id' => $model->id]);
            }

            var_dump($model->errors);
            die;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Logbook model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Logbook model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Logbook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Logbook the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Logbook::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
