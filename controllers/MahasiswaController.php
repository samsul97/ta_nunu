<?php

namespace app\controllers;

use app\models\Kelas;
use app\models\Pengampu;
use app\models\Semester;
use app\models\SemesterCalender;
use app\models\SemesterMahasiswa;
use app\models\User;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Yii;
use app\models\Mahasiswa;
use app\models\MahasiswaSearch;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MahasiswaController implements the CRUD actions for Mahasiswa model.
 */
class MahasiswaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Mahasiswa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MahasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mahasiswa model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Mahasiswa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Mahasiswa();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Mahasiswa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Mahasiswa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mahasiswa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Mahasiswa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mahasiswa::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionImport()
    {
        $modelImport = new \yii\base\DynamicModel([
            'fileImport' => 'File Import',
        ]);

        $modelImport->addRule(['fileImport'], 'required');
        $modelImport->addRule(['fileImport'], 'file', ['extensions' => 'xls,xlsx']);

        if (Yii::$app->request->post()) {
            ini_set('max_execution_time', 600);
            $modelImport->fileImport = \yii\web\UploadedFile::getInstance($modelImport, 'fileImport');

            if ($modelImport->fileImport && $modelImport->validate()) {
                $reader = new Xlsx();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($modelImport->fileImport->tempName);
                foreach($spreadsheet->getSheetNames() as $sheetName) {
                    $registeredYear = explode('(', $sheetName)[0];
                    $currentClass = str_replace(')', '',  explode('(', $sheetName)[1]);

                    // Get current semester
                    $semesterModel = Semester::findOne(1);
                    switch ($registeredYear) {
                        case 2018:
                            $semesterModel = Semester::findOne(3);
                            break;
                        case 2017:
                            $semesterModel = Semester::findOne(5);
                            break;
                        case 2016:
                            $semesterModel = Semester::findOne(7);
                            break;
                    }

                    // Get current class
                    $classModel = Kelas::find()->where(['nama'=>$currentClass])->one();
                    if($classModel === null) {
                        $classModel = new Kelas();
                        $classModel->nama = $currentClass;
                        $classModel->save();
                    }

                    $currentSheet = $spreadsheet->getSheetByName($sheetName);
                    $rowIterator = $currentSheet->getRowIterator(1, 100);
                    $header = [];
                    while($rowIterator->valid()) {
                        $currentRow = $rowIterator->current();
                        $cellIterator = $currentRow->getCellIterator('A', 'J');

                        $currentMahasiswaName = '';
                        $currentMahasiswaNim = '';
                        while($cellIterator->valid()) {
                            $currentCellValue = $cellIterator->current()->getValue();
                            if(count($header) === 2) {
                                if(array_key_exists($cellIterator->current()->getColumn(), $header)) {
                                    $currentCellIndex = $header[$cellIterator->current()->getColumn()];
                                    if($currentCellIndex === 'NIM') {
                                        $currentMahasiswaNim = $currentCellValue;
                                    } else {
                                        $currentMahasiswaName = $currentCellValue;
                                    }
                                }
                            } elseif(in_array(strtolower(trim($currentCellValue)), ['nim', 'nama'])) {
                                $header[$cellIterator->current()->getColumn()] = $currentCellValue;
                            }

                            $cellIterator->next();
                        }

                        if($currentMahasiswaName == '' || $currentMahasiswaNim == '') {
                            $rowIterator->next();
                            continue;
                        }

                        $mahasiswaModel = Mahasiswa::find()->where(['nim'=>$currentMahasiswaNim])->one();
                        if($mahasiswaModel === null) {
                            $userModel = new User();
                            $userModel->username = $currentMahasiswaName;
                            $userModel->password = Yii::$app->getSecurity()->generatePasswordHash($currentMahasiswaNim);
                            $userModel->id_user_role = 2;
                            $userModel->status = 1;
                            $userModel->token = '';
                            if(!$userModel->save()) {
                                echo "Error when creating user with nim: $currentMahasiswaNim and name: $currentMahasiswaName";
                                VarDumper::dump($userModel->getErrors());
                                die();
                            }

                            $semesterCalenderModel = SemesterCalender::find()->where(['tahun'=>$registeredYear, 'is_active'=>true])->one();
                            if($semesterCalenderModel === null) {
                                $semesterCalenderModel = SemesterCalender::find()->where(['tahun'=>$registeredYear, 'semester'=>'Semester Genap'])->one();
                                if($semesterCalenderModel === null) {
                                    $semesterCalenderModel = new SemesterCalender();
                                    $semesterCalenderModel->tahun = $registeredYear;
                                    $semesterCalenderModel->semester = 'Semester Genap';
                                    $semesterCalenderModel->save();
                                }

                                $semesterCalenderModel2 = SemesterCalender::find()->where(['tahun'=>$registeredYear, 'semester'=>'Semester Ganjil'])->one();
                                if($semesterCalenderModel2 === null) {
                                    $semesterCalenderModel2 = new SemesterCalender();
                                    $semesterCalenderModel2->tahun = $registeredYear;
                                    $semesterCalenderModel2->semester = 'Semester Ganjil';
                                    $semesterCalenderModel2->save();
                                }
                            }

                            $mahasiswaModel = new Mahasiswa();
                            $mahasiswaModel->nim = (String)$currentMahasiswaNim;
                            $mahasiswaModel->nama = $currentMahasiswaName;
                            $mahasiswaModel->tahun = $registeredYear;
                            $mahasiswaModel->user_id = $userModel->id;
                            if(!$mahasiswaModel->save()) {
                                echo "Error when creating mahasiswa with nim: $currentMahasiswaNim and name: $currentMahasiswaName";
                                VarDumper::dump($mahasiswaModel->getErrors());
                                die();
                            }

                            $semesterMahasiswa = new SemesterMahasiswa();
                            $semesterMahasiswa->kelas_id = $classModel->id;
                            $semesterMahasiswa->mahasiswa_id = $mahasiswaModel->id;
                            $semesterMahasiswa->semester_kalender_id = $semesterCalenderModel->id;
                            if(!$semesterMahasiswa->save()) {
                                echo "Error when creating semester mahasiswa with nim: $currentMahasiswaNim and name: $currentMahasiswaName";
                                VarDumper::dump($semesterMahasiswa);
                                die();
                            }
                        }
                        $rowIterator->next();
                    }
                }
                Yii::$app->getSession()->setFlash('success', 'Success');
                return $this->redirect(['mahasiswa/index']);
            }

            Yii::$app->getSession()->setFlash('error', 'Error');
        }

        return $this->render('import', [
            'modelImport' => $modelImport,
        ]);
    }
}
