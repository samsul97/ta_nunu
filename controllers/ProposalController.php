<?php

namespace app\controllers;

use app\models\Proposal;
use app\models\ProposalSearch;
use app\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProposalController implements the CRUD actions for Proposal model.
 */
class ProposalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Proposal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProposalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (in_array(Yii::$app->user->identity->id_user_role, [2, 3], true)) {
            $kelas = Yii::$app->user->identity->semesterMahasiswa->kelas_id;
            $kelompok = Yii::$app->user->identity->semesterMahasiswa->kelompok_id;

            if (Yii::$app->user->identity->id_user_role == 2) {
                $dataProvider->query->where(['semester_mahasiswa.kelas_id' => $kelas, 'semester_mahasiswa.kelompok_id' => $kelompok]);
            }
            if (User::isKordinator()) {
                $dataProvider->query->where(['semester_mahasiswa.kelas_id' => $kelas]);
            }
        }

        $couldCreate = true;
        foreach ($dataProvider->models as $model) {
            if ($model->status === 1) {
                $couldCreate = false;
                break;
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'couldCreateNew' => $couldCreate
        ]);
    }

    /**
     * Displays a single Proposal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Proposal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_mhs = null, $id_kelompok = null, $id_kelas = null)
    {
        $model = new Proposal();
        // Variabel
        $model->id_mhs = $id_mhs;
        $model->id_kelompok = $id_kelompok;
        $model->id_kelas = $id_kelas;

        if (Yii::$app->user->identity->id_user_role == 2) {
            $model->id_mhs = Yii::$app->user->identity->id_mahasiswa;
            $model->id_kelompok = Yii::$app->user->identity->id_kelompok;
            $model->id_kelas = Yii::$app->user->identity->id_kelas;

        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->time = date('Y-m-d');
            $file = UploadedFile::getInstance($model, 'file_upload');
            $model->file = time() . '_' . $file->name;
            $model->save(false);
            $file->saveAs(Yii::$app->basePath . '/web/upload/' . $model->file);
            Yii::$app->session->setFlash('success', 'Berhasil menambahkan data');
            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Proposal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $file_lama = $model->file;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $file = UploadedFile::getInstance($model, 'file_upload');
            if ($file !== null) {
                unlink(Yii::$app->basePath . '/web/upload/' . $file_lama);
                $model->file = time() . '_' . $file->name;
                $file->saveAs(Yii::$app->basePath . '/web/upload/' . $model->file);
            } else {
                $model->file = $file_lama;
            }
            $model->save(false);
            Yii::$app->session->setFlash('success', 'Data berhasil di Edit');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Proposal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Proposal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Proposal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Proposal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEditStatus($id, $status)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['proposal/index']);
            }

            VarDumper::dump($model->getErrors());
            die();
        }

        $model->status = $status;
        if ($model->save()) {
            Yii::$app->session->setFlash('Berhasil', 'Status telah di Ubah');
            if ($status == 2) {
                return $this->render('reason', [
                    'model' => $model
                ]);
            }
            return $this->redirect(['proposal/index']);
        } else {
            echo "Yah eror";
            var_dump($model->errors);
            die;
        }
    }
}
