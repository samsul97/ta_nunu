<?php

namespace app\controllers;

use app\models\User;
use Yii;
use app\models\Revisi;
use app\models\RevisiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\Proposal;

/**
 * RevisiController implements the CRUD actions for Revisi model.
 */
class RevisiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Revisi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RevisiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $kelas = Yii::$app->user->identity->semesterMahasiswa->kelas_id;
        $kelompok = Yii::$app->user->identity->semesterMahasiswa->kelompok_id;
        $mahasiswa = Yii::$app->user->identity->semesterMahasiswa->mahasiswa_id;

        if (Yii::$app->user->identity->id_user_role == 2)
        {
            $dataProvider->query->where(['semester_mahasiswa.kelas_id' => $kelas, 'semester_mahasiswa.kelompok_id' => $kelompok]);
        }
        if (User::isKordinator())
        {
            $dataProvider->query->where(['semester_mahasiswa.kelas_id' => $kelas]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Revisi model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Revisi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_mhs=null, $id_kelompok=null, $id_kelas=null)
    {
        $model = new Revisi();
        $model->id_mhs = $id_mhs;
        $model->id_kelompok = $id_kelompok;
        $model->id_kelas = $id_kelas;

        if (Yii::$app->user->identity->id_user_role == 2)
        {
            $model->id_mhs = Yii::$app->user->identity->id_mahasiswa;
            $model->id_kelompok = Yii::$app->user->identity->id_kelompok;
            $model->id_kelas = Yii::$app->user->identity->id_kelas;
        }
        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            $file = UploadedFile::getInstance($model, 'file_upload');
            $model->file = time(). '_' . $file->name;
            $model->save(false);
            $file->saveAs(Yii::$app->basePath. '/web/upload/' . $model->file);
            Yii::$app->session->setFlash('success', 'Berhasil menambahkan data');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Revisi model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $file_lama = $model->file;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $file = UploadedFile::getInstance($model, 'file_upload');
            if ($file !== null) {
                unlink(Yii::$app->basePath . '/web/upload/' . $file_lama);
                $model->file = time() . '_' . $file->name;
                $file->saveAs(Yii::$app->basePath . '/web/upload/' . $model->file);
            } else{
                $model->file = $file_lama;
            }
            $model->save(false);
            Yii::$app->session->setFlash('success', 'Data berhasil diperbaharui');
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Revisi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Revisi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Revisi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Revisi::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
