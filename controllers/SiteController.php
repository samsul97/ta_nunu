<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\Kordinator;
use app\models\LoginForm;
use app\models\Mahasiswa;
use app\models\Pengampu;
use app\models\SemesterCalender;
use app\models\SemesterMahasiswa;
use app\models\SemesterPengampu;
use app\models\TambahAkun;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\Response;

// use yii\data\ActiveDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['site/dashboard']);
        } else {
            return $this->redirect(['site/login']);
        }
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionDashboard()
    {
        if (User::isAdmin() || User::isMahasiswa() || User::isKordinator() || User::isPengampu()) {
            $provider = new ActiveDataProvider([
                'query' => \app\models\Informasi::find(),
                'pagination' => [
                    'pageSize' => 6
                ],
            ]);
            return $this->render('dashboard', ['provider' => $provider]);
        } else {
            return $this->redirect('site/login');
        }
    }

    public function actionTambahAkun()
    {
        $model = new TambahAkun();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $semesterCalender = SemesterCalender::find()->where(['id'=>$model->semester_calender_id])->one();
            if ($model->id_user_role == 2) {
                $user = $this->createNewUser($model);

                $mahasiswa = new Mahasiswa();
                $mahasiswa->nim = $model->identity_number;
                $mahasiswa->nama = $model->nama;
                $mahasiswa->user_id = $user->id;
                $mahasiswa->tahun = $semesterCalender->tahun;

                if (!$mahasiswa->save()) {
                    Yii::$app->session->setFlash('success', 'Nim dan Username tidak boleh sama');
                    var_dump($mahasiswa->errors);
                    die;
                }

                $semesterMahasiswa = new SemesterMahasiswa();
                $semesterMahasiswa->semester_kalender_id = $model->semester_calender_id;
                $semesterMahasiswa->kelas_id = $model->id_kelas;
                $semesterMahasiswa->kelompok_id = $model->id_kelompok;
                $semesterMahasiswa->mahasiswa_id = $mahasiswa->id;
                if (!$semesterMahasiswa->save()) {
                    VarDumper::dump($semesterMahasiswa->getErrors());
                    die();
                }

                Yii::$app->session->setFlash('success', 'Berhasil Tambah Akun mahasiswa.');
                return $this->redirect(['mahasiswa/index']);
            }

            if ($model->id_user_role == 3) {
                $user = $this->createNewUser($model);
                $pengampu = new Pengampu();
                $pengampu->nama = $model->nama;
                $pengampu->nik = $model->identity_number;
                $pengampu->is_coordinator = $model->is_coordinator;
                $pengampu->user_id = $user->id;
                if (!$pengampu->save()) {
                    echo 'Error di pengampu<br>';
                    var_dump($pengampu->errors);
                    die;
                }

                $semesterPengampu = new SemesterPengampu();
                $semesterPengampu->kelas_id = $model->id_kelas;
                $semesterPengampu->semester_kalender_id = $semesterCalender->id;
                $semesterPengampu->pengampu_id = $pengampu->id;
                if(!$semesterPengampu->save()) {
                    echo 'Error di pengampu<br>';
                    var_dump($semesterPengampu->errors);
                    die;
                }

                Yii::$app->session->setFlash('success', 'Berhasil Tambah Akun pengampu.');
                return $this->redirect(['pengampu/index']);
            }

            return $this->redirect(['site/tambah-akun']);
        }
        return $this->render('akun', [
            'model' => $model,
        ]);
    }

    private function createNewUser(TambahAkun $model): User
    {
        $user = new User();
        $user->username = $model->username;
        $user->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
        $user->id_user_role = $model->id_user_role;
        $user->status = 1;
        $user->token = Yii::$app->getSecurity()->generateRandomString(100);
        if (!$user->save()) {
            VarDumper::dump($user->getErrors());
            die();
        }

        return $user;
    }
}