<?php

use app\models\User;
use yii\helpers\ArrayHelper;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php if (User::isAdmin()): ?>
                    <?= User::getFotoAdmin(['class' => 'img-circle']); ?>
                <?php endif ?>
                <?php if (User::isMahasiswa()): ?>
                    <?= User::getFotoMahasiswa(['class' => 'img-circle']); ?>
                <?php endif ?>
                <?php if (User::isKordinator()): ?>
                    <?= User::getFotoKordinator(['class' => 'img-circle']); ?>
                <?php endif ?>
                <?php if (User::isPengampu()): ?>
                    <?= User::getFotoPengampu(['class' => 'img-circle']); ?>
                <?php endif ?>
            </div>
            <div class="pull-left info">
                <p>
                    <?= Yii::$app->user->identity->username ?>
                </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <?php if (User::isAdmin()) { ?>
            <!-- Ini Tampil ADMIN -->
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                        ['label' => 'Menu Aplikasi', 'options' => ['class' => 'header']],
                        ['label' => 'Beranda', 'icon' => 'dashboard', 'url' => ['site/dashboard']],
                        ['label' => 'Tambah Akun', 'icon' => 'user', 'url' => ['site/tambah-akun']],
                        ['label' => 'Informasi', 'icon' => 'pencil', 'url' => ['informasi/index']],
                        [
                            'label' => 'Menu Mahasiswa',
                            'icon' => 'user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Proposal', 'icon' => 'book', 'url' => ['proposal/index']],
                                ['label' => 'Logbook', 'icon' => 'file', 'url' => ['logbook/index']],
                                ['label' => 'Revisi', 'icon' => 'window-restore', 'url' => ['revisi/index']],
                            ],
                        ],
                        [
                            'label' => 'Data User',
                            'icon' => 'user',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Mahasiswa', 'icon' => 'users', 'url' => ['mahasiswa/index']],
                                ['label' => 'Pengampu', 'icon' => 'users', 'url' => ['pengampu/index']],
                            ],
                        ],
                        [
                            'label' => 'Data Master',
                            'icon' => 'list',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Kelas', 'icon' => 'circle-o', 'url' => ['kelas/index']],
                                ['label' => 'Semester', 'icon' => 'circle-o', 'url' => ['semester/index']],
                                ['label' => 'Kelompok', 'icon' => 'circle-o', 'url' => ['kelompok/index']],
                            ],
                        ],
                    ],
                ]
            ) ?>
        <?php } elseif (User::isMahasiswa()) { ?>
            <?= dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => [
                        ['label' => 'Menu Aplikasi', 'options' => ['class' => 'header']],
                        ['label' => 'Beranda', 'icon' => 'dashboard', 'url' => ['site/dashboard']],
                        ['label' => 'Proposal', 'icon' => 'book', 'url' => ['proposal/index']],
                        ['label' => 'Logbook', 'icon' => 'file', 'url' => ['logbook/index']],
                        ['label' => 'Revisi', 'icon' => 'window-restore', 'url' => ['revisi/index']],
                        ['label' => 'Kelompok', 'icon' => 'users', 'url' => ['kelompok/index']],
                        ['label' => 'Informasi', 'icon' => 'pencil', 'url' => ['informasi/index']],
                        ['label' => 'Comments', 'icon' => 'comments', 'url' => ['comments/index']],

                    ],
                ]
            ) ?>
        <?php } elseif (User::isPengampu()) {
            $menuItems = [
                ['label' => 'Menu Pengampu', 'options' => ['class' => 'header']],
                ['label' => 'Beranda', 'icon' => 'dashboard', 'url' => ['site/dashboard']],
                ['label' => 'Kelompok', 'icon' => 'users', 'url' => ['kelompok/index']],
                ['label' => 'Proposal', 'icon' => 'book', 'url' => ['proposal/index']],
                ['label' => 'Logbook', 'icon' => 'file', 'url' => ['logbook/index']],
                ['label' => 'Revisi', 'icon' => 'window-restore', 'url' => ['revisi/index']],
            ];

            if (User::isKordinator()) {
                $menuItems = ArrayHelper::merge($menuItems, [
                    ['label' => 'Informasi', 'icon' => 'pencil', 'url' => ['informasi/index']],
                    ['label' => 'Pengampu', 'icon' => 'users', 'url' => ['pengampu/index']],
                ]);
            }

            echo dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                    'items' => $menuItems,
                ]
            );

        } ?>
    </section>
</aside>