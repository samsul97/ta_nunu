<?php

use app\models\Comments;use app\models\User;
use Codeception\Lib\Notification;use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<!-- Tasks: style can be found in dropdown.less -->
<li class="dropdown tasks-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-flag-o"></i>
        <span class="label label-danger"><?= Comments::find()->count() ?></span>
    </a>
    <ul class="dropdown-menu">
        <li class="header">You have <?= Comments::find()->count() ?> tasks</li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <?php $comments = Comments::find()->limit(5)->all(); ?>
                <?php foreach ($comments as $comment) : ?>
                <li><!-- Task item -->
                    <a href="#">
                        <h3 style="font-size: .7em;">
                            <?= $comment->comment ?>
                        </h3>
                    </a>
                </li>
                <!-- end task item -->
                <?php endforeach ?>
            </ul>
        </li>
        <li class="footer">
            <a href="#">View all tasks</a>
        </li>
    </ul>
</li>