<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "informasi".
 *
 * @property int $id
 * @property string $judul
 * @property string $isi
 * @property string $tanggal
 * @property string $keterangan
 * @property string $tempat
 */
class Informasi extends \yii\db\ActiveRecord
{
    public $image_upload;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'informasi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul', 'keterangan', 'tempat'], 'required'],
            [['tanggal'], 'safe'],
            [['keterangan'], 'string'],
            [['judul', 'tempat'], 'string', 'max' => 100],
            [['image_upload'], 'image', 'extensions'=>'png, jpg, jpeg, avg, gif'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul',
            'tanggal' => 'Tanggal',
            'keterangan' => 'Keterangan',
            'tempat' => 'Tempat',
            'image' => 'Image',
        ];
    }
    public function getCount()
   {
    return static::find()->count();
}
}
