<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kelompok".
 *
 * @property int $id
 * @property mixed $list
 * @property mixed $count
 * @property string $nama_kelompok [varchar(400)]
 */
class Kelompok extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kelompok';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_kelompok'], 'required'],
            [['nama_kelompok'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_kelompok' => 'Nama Kelompok',
        ];
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'nama_kelompok');
    }

    public function getCount()
    {
        return static::find()->count();
    }


    public static function findAllKelompok($kelompokId)
    {
        $query = Mahasiswa::find()->joinWith(['currentSemesterData'])->where(['semester_mahasiswa.kelompok_id' => $kelompokId])->orderBy('semester_mahasiswa.kelas_id');
        if (Yii::$app->user->identity->id_user_role === 2) {
            return $query
                ->andWhere(['semester_mahasiswa.kelompok_id' => Yii::$app->user->identity->semesterMahasiswa->kelompok_id, 'semester_mahasiswa.kelas_id' => Yii::$app->user->identity->semesterMahasiswa->kelas_id])
                ->all();
        }
        if (User::isKordinator()) {
            return $query
                ->andWhere(['semester_mahasiswa.kelas_id' => Yii::$app->user->identity->semesterMahasiswa->kelas_id])
                ->all();
        }

        return [];
    }
}
