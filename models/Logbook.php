<?php

namespace app\models;

/**
 * This is the model class for table "logbook".
 *
 * @property int $id
 * @property int $id_proposal
 * @property string $judul
 * @property string $isi
 * @property string $keterangan
 * @property string $time
 * @property int $id_semester_mhs [int(11)]
 */
class Logbook extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'logbook';
    }

    public static function find()
    {
        return parent::find()->joinWith(['mahasiswa']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_proposal', 'id_semester_mhs', 'judul', 'time'], 'required'],
            [['id_proposal', 'id_semester_mhs'], 'integer'],
            [['time'], 'safe'],
            [['judul', 'isi', 'keterangan'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_kelas' => 'Id Kelas',
            'id_kelompok' => 'Id Kelompok',
            'id_proposal' => 'Id Proposal',
            'id_semester_mhs' => 'Id Mhs',
            'judul' => 'Judul',
            'isi' => 'Isi',
            'keterangan' => 'Keterangan',
            'time' => 'Time',
        ];
    }

    public function getCount()
    {
        return static::find()->count();
    }

    public function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'mahasiswa.nama');
    }

    public function getSemesterMahasiswa()
    {
        return $this->hasMany(SemesterMahasiswa::class, ['mahasiswa_id' => 'id']);
    }

    public function getCurrentSemesterMahasiswa() {
        return $this->getSemesterMahasiswa()
            ->join('INNER JOIN', 'semester_kalender',
                'semester_kalender.id=semester_mahasiswa.semester_kalender_id and semester_kalender.is_active=1');
    }

    public function getMahasiswa()
    {
        return $this->hasOne(Mahasiswa::class, ['id' => 'mahasiswa_id'])->via('currentSemesterMahasiswa');
    }

    public function getProposal()
    {
        return $this->hasOne(Proposal::class, ['id' => 'id_proposal']);
    }
}
