<?php

namespace app\models;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property int $id
 * @property string $nama
 * @property string $nim [varchar(10)]
 * @property mixed $list
 * @property mixed $semester
 * @property mixed $count
 * @property mixed $kelas
 * @property mixed $kelompok
 * @property string $tahun [char(4)]
 * @property mixed $user
 * @property int $user_id [int(11)]
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nim', 'nama'], 'required'],
            [['nim', 'tahun'], 'number'],
            [['nim', 'nama'], 'string', 'max' => 100],
            // [['nim'], 'unique', 'targetClass' => '\app\models\Mahasiswa'],
            // ['nama', 'match', 'pattern' => '/^[a-z]\w*$/i', 'message' => 'Huruf Harus Jelas'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nim' => 'NIM',
            'nama' => 'Nama',
            // 'id_kelas' => 'Kelas',
            // 'id_kelompok' => 'Kelompok',
            'tahun' => 'Tahun Angkatan',
        ];
    }

    public function getSemesterData()
    {
        return $this->hasMany(SemesterMahasiswa::class, ['mahasiswa_id' => 'id']);
    }

    public function getCurrentSemesterData() {
        return $this->getSemesterData()
            ->alias('a_semester_mahasiswa')
            ->join('INNER JOIN', 'semester_mahasiswa', [
                'a_semester_mahasiswa.semester_kalender_id'=>'id'
            ])
            ->join('INNER JOIN', 'semester_kalender', [
                'a_semester_mahasiswa.semester_kalender_id'=>'id',
                'semester_kalender.is_active'=>true
            ]);
    }

    public function getKelas()
    {
        return $this->getCurrentSemesterData()->one()->getKelas();
    }

    public function getKelompok()
    {
        return $this->getCurrentSemesterData()->one()->getKelompok();
    }

    public function getSemester()
    {
        if($this->getSemesterData()) {
            return $this->getCurrentSemesterData()->one()->getSemesterCalender();
        }

        return null;
    }

    public static function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'nama');
    }

    public function getCount()
    {
        return static::find()->count();
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id_mahasiswa' => 'id']);
    }
}
