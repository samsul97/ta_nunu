<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengampu".
 *
 * @property int $id
 * @property string $nik
 * @property string $nama
 * @property bool $is_coordinator [tinyint(1)]
 * @property int $user_id [int(11)]
 */
class Pengampu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pengampu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nik', 'nama'], 'required'],
            [['nik'], 'number'],
            [['nik', 'nama'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nik' => 'Nik',
            'nama' => 'Nama',
        ];
    }
    public function getList()
    {
     return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', 'nama');
    }
    public function getCount()
    {
        return static::find()->count();
    }

    public static function getCoordinatorCount() {
        return static::find()->where(['is_coordinator'=>true])->count();
    }
}
