<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proposal".
 *
 * @property int $id
 * @property string $judul
 * @property string $file
 * @property int $status [int(11)]
 * @property mixed $count
 * @property mixed $mahasiswa
 * @property string $time [date]
 * @property mixed $semesterMahasiswa
 * @property mixed $currentSemesterMahasiswa
 * @property int $id_semester_mhs [int(11)]
 * @property string $declined_reason [varchar(500)]
 */
class Proposal extends \yii\db\ActiveRecord
{
    public $file_upload;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proposal';
    }

    public static function find()
    {
        return parent::find()->joinWith(['mahasiswa'])
            ->andWhere(['semester_kalender.is_active'=>true]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mhs', 'judul'], 'required'],
            [['id_mhs', 'status'], 'integer'],
            [['judul'], 'string', 'max' => 100],
            [['declined_reason'], 'string', 'max' => 500],
            [['time'], 'safe'],
            [['file_upload'], 'file', 'extensions' => 'docx, doc, pdf, xls, xlsx'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_mhs' => 'Mahasiswa',
            'id_kelompok' => 'Kelompok',
            'judul' => 'Judul',
            'file' => 'File',
            'status' => 'Status',
            'time' => 'Time',
            'semesterMahasiswa.semesterCalender.tahun' => 'Tahun',
            'semesterMahasiswa.semesterCalender.semester' => 'Semester'
        ];
    }

    public static function getList()
    {
        // $Mahasiswa = Mahasiswa::find()->andWhere(['id' => Yii::$app->user->identity->id_mahasiswa]);

        if (User::isMahasiswa()) {
            return \yii\helpers\ArrayHelper::map(self::find()->andWhere(['semester_mahasiswa.mahasiswa_id' => Yii::$app->user->identity->semesterMahasiswa->mahasiswa_id])->all(), 'id', 'judul');
        }
    }

    public function getCount()
    {
        return static::find()->count();
    }

    public function getSemesterMahasiswa()
    {
        return $this->hasMany(SemesterMahasiswa::class, ['mahasiswa_id' => 'id']);
    }

    public function getCurrentSemesterMahasiswa() {
        return $this->getSemesterMahasiswa()
            ->join('INNER JOIN', 'semester_kalender',
                'semester_kalender.id=semester_mahasiswa.semester_kalender_id and semester_kalender.is_active=1');
    }

    public function getMahasiswa()
    {
        return $this->hasOne(Mahasiswa::class, ['id' => 'mahasiswa_id'])->via('currentSemesterMahasiswa');
    }
}