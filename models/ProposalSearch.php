<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Proposal;

/**
 * ProposalSearch represents the model behind the search form of `app\models\Proposal`.
 */
class ProposalSearch extends Proposal
{
    public $id_kelompok;
    public $id_kelas;
    public $id_mhs;
    public $tahun;
    public $semester;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_semester_mhs', 'id_kelompok', 'id_kelas', 'tahun'], 'integer'],
            [['judul', 'semester'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Proposal::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_semester_mhs' => $this->id_semester_mhs,
            'id_kelompok' => $this->id_kelompok,
            'id_kelas' => $this->id_kelas,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'file', $this->file]);

        return $dataProvider;
    }
}
