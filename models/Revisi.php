<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "revisi".
 *
 * @property int $id
 * @property string $judul
 * @property int $id_kelompok
 * @property int $id_proposal
 * @property string $file
 * @property string $deskripsi
 */
class Revisi extends \yii\db\ActiveRecord
{
    public $file_upload;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'revisi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul', 'id_kelompok', 'id_proposal', 'id_kelas', 'id_mhs', 'deskripsi'], 'required'],
            [['id_kelompok', 'id_kelas', 'id_mhs', 'id_proposal'], 'integer'],
            [['judul', 'deskripsi'], 'string', 'max' => 100],
            [['file_upload'], 'file', 'extensions'=>'docx, doc, pdf, xls, xlsx'],
            // [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'judul' => 'Judul Revisi',
            'id_kelompok' => 'Kelompok',
            'id_proposal' => 'Acuan Proposal',
            'id_kelas' => 'Kelas',
            'id_mhs' => 'Mahasiswa',
            'file_upload' => 'File',
            'deskripsi' => 'Deskripsi',
        ];
    }
    public function getCount()
    {
        return static::find()->count();
    }
    public function getKelompok()
    {
        return $this->hasOne(Kelompok::class, ['id' => 'id_kelompok']);
    }
    public function getProposal()
    {
        return $this->hasOne(Proposal::class, ['id' => 'id_proposal']);
    }
    public function getKelas()
    {
        return $this->hasOne(Kelas::class, ['id' => 'id_kelas']);
    }
    public function getMahasiswa()
    {
        return $this->hasOne(Mahasiswa::class, ['id' => 'id_mhs']);
    }

    public function getSemesterMahasiswa() {
        return $this->hasOne(SemesterMahasiswa::class, ['id'=> 'id_semester_mhs']);
    }
}
