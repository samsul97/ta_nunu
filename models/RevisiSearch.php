<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Revisi;

/**
 * RevisiSearch represents the model behind the search form of `app\models\Revisi`.
 */
class RevisiSearch extends Revisi
{
    public $id_kelompok;
    public $id_kelas;
    public $id_mhs;
    public $id_proposal;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_semester_mhs'], 'integer'],
            [['judul', 'file', 'deskripsi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Revisi::find()->joinWith(['semesterMahasiswa']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'id_kelompok' => $this->id_kelompok,
            // 'id_proposal' => $this->id_proposal,
            // 'id_kelas' => $this->id_kelas,
            'id_semester_mhs' => $this->id_semester_mhs,
        ]);

        $query->andFilterWhere(['like', 'judul', $this->judul])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi]);

        return $dataProvider;
    }
}
