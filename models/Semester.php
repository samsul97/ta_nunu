<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "semester_calender".
 *
 * @property int $id
 * @property string $tahun
 * @property string $semester
 * @property string $dari_tgl
 * @property string $ke_tgl
 * @property bool $is_active [tinyint(1)]
 */
class Semester extends SemesterCalender
{

}
