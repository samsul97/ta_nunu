<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "semester_calender".
 *
 * @property int $id
 * @property string $tahun
 * @property string $semester
 * @property string $dari_tgl
 * @property string $ke_tgl
 * @property bool $is_active [tinyint(1)]
 */
class SemesterCalender extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'semester_kalender';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['dari_tgl', 'ke_tgl'], 'safe'],
            [['tahun'], 'string', 'max' => 16],
            [['semester'], 'string', 'max' => 400],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tahun' => 'Tahun',
            'semester' => 'Semester',
            'dari_tgl' => 'Dari Tgl',
            'ke_tgl' => 'Ke Tgl',
        ];
    }

    public function getCount()
    {
        return static::find()->count();
    }

    public static function getList()
    {
        return \yii\helpers\ArrayHelper::map(self::find()->all(), 'id', static function(SemesterCalender $model) {
            return $model->tahun . ' - ' . $model->semester;
        });
    }

    public static function getYearList() {
        return \yii\helpers\ArrayHelper::map(self::find()->groupBy('tahun')->all(), 'id', static function(SemesterCalender $model) {
            return $model->tahun;
        });
    }

    public static function getSemesterList() {
        return \yii\helpers\ArrayHelper::map(self::find()->groupBy('semester')->all(), 'id', static function(SemesterCalender $model) {
            return $model->semester;
        });
    }
}
