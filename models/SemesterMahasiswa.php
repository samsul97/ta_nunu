<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "semester_mahasiswa".
 *
 * @property int $id
 * @property int $kelompok_id
 * @property int $mahasiswa_id
 * @property int $kelas_id
 * @property int $semester_kalender_id [int(11)]
 */
class SemesterMahasiswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'semester_mahasiswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['semester_kalender_id', 'kelompok_id', 'mahasiswa_id', 'kelas_id'], 'integer'],
            [['mahasiswa_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'semester_kalender_id' => 'Semester Calender ID',
            'kelompok_id' => 'Kelompok ID',
            'mahasiswa_id' => 'Mahasiswa ID',
            'kelas_id' => 'Kelas ID',
        ];
    }

    public function getMahasiswa()
    {
        return $this->hasOne(Mahasiswa::class, ['id' => 'mahasiswa_id']);
    }

    public function getKelompok()
    {
        return $this->hasOne(Kelompok::class, ['id' => 'kelompok_id']);
    }

    public function getKelas()
    {
        return $this->hasOne(Kelas::class, ['id' => 'kelas_id']);
    }

    public function getSemesterCalender()
    {
        return $this->hasOne(SemesterCalender::class, ['id' => 'semester_kalender_id']);
    }
}
