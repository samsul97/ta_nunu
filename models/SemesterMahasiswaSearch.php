<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SemesterMahasiswa;

/**
 * SemesterMahasiswaSearch represents the model behind the search form of `app\models\SemesterMahasiswa`.
 */
class SemesterMahasiswaSearch extends SemesterMahasiswa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'semester_kalender_id', 'kelompok_id', 'mahasiswa_id', 'kelas_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SemesterMahasiswa::find()->joinWith(['semesterCalender'])
        ->where(['semester_kalender.is_active'=> true]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'semester_kalender_id' => $this->semester_kalender_id,
            'kelompok_id' => $this->kelompok_id,
            'mahasiswa_id' => $this->mahasiswa_id,
            'kelas_id' => $this->kelas_id,
        ]);

        return $dataProvider;
    }
}
