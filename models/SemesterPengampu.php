<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "semester_pengampu".
 *
 * @property int $id
 * @property int $kelas_id
 * @property int $semester_kalender_id
 * @property int $pengampu_id [int(11)]
 */
class SemesterPengampu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'semester_pengampu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'kelas_id', 'semester_kalender_id', 'pengampu_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kelas_id' => 'Kelas ID',
            'semester_kalender_id' => 'Semester Kalender ID',
            'pengampu_id' => 'User ID',
        ];
    }

    public function getPengampu()
    {
        return $this->hasOne(Pengampu::class, ['id' => 'pengampu_id']);
    }

    public function getKelas()
    {
        return $this->hasOne(Kelas::class, ['id' => 'kelas_id']);
    }

    public function getSemesterCalender()
    {
        return $this->hasOne(SemesterCalender::class, ['id' => 'semester_kalender_id']);
    }
}
