<?php
namespace app\models;
use Yii;
use yii\base\Model;

/**
 * LoginForm is the model basename(path)ehind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class TambahAkun extends Model
{
    // public $id_jurusan;
    // public $id_hak_akses;
    public $id_user_role;
    public $id_kelas;
    public $id_kelompok;
    public $is_coordinator;
    public $username;
    public $password;
    public $nama;
    public $identity_number;
    public $semester_calender_id;

    public function rules()
    {
        return [
            ['username', 'match', 'pattern' => '/^[a-z]\w*$/i', 'message' => 'Username salah'],
            [['username'], 'unique', 'targetClass' => '\app\models\User'],
            [['password'], 'string', 'min' => 5],
            [['id_user_role', 'id_kelas', 'id_kelompok', 'semester_calender_id'], 'integer'],
            [['nama', 'identity_number'], 'required', 'message'=> 'Data tidak boleh kosong'],
            [['is_coordinator'], 'safe'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id_user_role' => 'User',
            'is_coordinator' => 'Koordinator',
            'username' => 'Username',
            'password' => 'Password',
            'nama' => 'Nama',
            'id_kelas' => 'Kelas',
            'id_kelompok' => 'Kelompok',
            'identity_number' => 'Nim/Nik',
        ];
    }   
}