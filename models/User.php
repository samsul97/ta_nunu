<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property int $id_user_role
 * @property int $status
 * @property string $token [varchar(100)]
 * @property int $id_jurusan [int(11)]
 * @property int $id_dosen [int(11)]
 * @property int $id_akademik [int(11)]
 * @property int $id_ketuajurusan [int(11)]
 * @property int $id_keuangan [int(11)]
 * @property mixed $userRole
 * @property mixed $kelas
 * @property mixed $mahasiswa
 * @property mixed $kelompok
 * @property null $authKey
 * @property int $id_wadir [int(11)]
 */
// Pada user model kegunaan dari masing-masing syntax adalah Autentikasi yang berarti untuk memverifikasi atau konfirmasi untuk mengamankan data pengguna.

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{

    public static function tableName()
    {
        return 'user';
    }

    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            [['id_user_role', 'status'], 'integer'],
            [['username', 'token'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 100],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'id_user_role' => 'User Role',
            'status' => 'Status',
            'token' => 'Token',
        ];
    }

    public function getMahasiswa()
    {
        return $this->hasOne(Mahasiswa::class, ['user_id' => 'id']);
    }

    public function getKelas()
    {
        return $this->hasOne(Kelas::class, ['id' => 'id_kelas']);
    }

    public function getKelompok()
    {
        return $this->hasOne(Kelompok::class, ['id' => 'id_kelompok']);
    }

    public function getPengampu()
    {
        return $this->hasOne(Pengampu::class, ['user_id' => 'id']);
    }

    public function getUserRole()
    {
        return $this->hasOne(UserRole::class, ['id' => 'id_user_role']);
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $Type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return null;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    public function validatePassword($password)
    {
        // return $this->password == $password;
        return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }

    public static function isAdmin()
    {
        return !Yii::$app->user->isGuest && Yii::$app->user->identity->id_user_role == 1;
    }

    public static function isMahasiswa()
    {
        return !Yii::$app->user->isGuest && Yii::$app->user->identity->id_user_role == 2;
    }

    public static function isKordinator()
    {
        return !Yii::$app->user->isGuest && Yii::$app->user->identity->id_user_role == 3 && Yii::$app->user->identity->pengampu->is_coordinator == 1;
    }

    public static function isPengampu()
    {
        return !Yii::$app->user->isGuest && Yii::$app->user->identity->id_user_role == 3;
    }

    public static function getFotoAdmin($htmlOptions = [])
    {
        return Html::img('@web/user/admin.jpg', $htmlOptions);
    }

    public static function getFotoMahasiswa($htmlOptions = [])
    {
        return Html::img('@web/user/original.jpg', $htmlOptions);
    }

    public static function getFotoKordinator($htmlOptions = [])
    {
        return Html::img('@web/user/original.jpg', $htmlOptions);
    }

    public static function getFotoPengampu($htmlOptions = [])
    {
        return Html::img('@web/user/original.jpg', $htmlOptions);
    }

    public function getSemesterMahasiswa() {
        $mahasiswa = Mahasiswa::find()
            ->join("INNER JOIN", "semester_kalender", "semester_kalender.id=mahasiswa.id_semester and semester_kalender.is_active=true")
            ->where(['mahasiswa.user_id'=>$this->id])
            ->one();

        if($mahasiswa !== null) {
            return $mahasiswa->getCurrentSemesterData()->one();
        }

        return new SemesterMahasiswa();
    }
}