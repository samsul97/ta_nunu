-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2019 at 04:02 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta_nunu_fix`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `comment` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`) VALUES
(1, 'Saya bingung kenapa kurikulumnya tidak mengacu pada kurikulum 2013?');

-- --------------------------------------------------------

--
-- Table structure for table `informasi`
--

CREATE TABLE `informasi` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `keterangan` text NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `informasi`
--

INSERT INTO `informasi` (`id`, `judul`, `tanggal`, `keterangan`, `tempat`, `image`) VALUES
(1, 'Expo Proyek IV', '2019-08-21', 'Kegiatan expo adalah kegiatan rutin mahasiswa untuk mempresentasikan hasil pembelajaranya selama melaksanakan mata kuliah proyek I', 'Depan Gedung TI', '1566392365_34069048_2065285883541691_2633774061594869760_n.jpg'),
(2, 'Expo Proyek V', '2019-08-21', 'Kegiatan expo adalah kegiatan rutin mahasiswa untuk mempresentasikan hasil pembelajaranya selama melaksanakan mata kuliah proyek V', 'Belakang Direktorat', '1566392391_34048615_1571123122986203_7500382722800484352_n.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `nama`) VALUES
(1, 'D3TI1A'),
(2, 'D3TI1B'),
(3, 'D3TI1C'),
(4, 'D3TI1D'),
(5, 'D3TI2A'),
(6, 'D3TI2B'),
(7, 'D3TI2C'),
(8, 'D3TI2D'),
(9, 'D3TI3A'),
(10, 'D3TI3B'),
(11, 'D3TI3C'),
(12, 'D3TI3D');

-- --------------------------------------------------------

--
-- Table structure for table `kelompok`
--

CREATE TABLE `kelompok` (
  `id` int(11) NOT NULL,
  `nama_kelompok` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelompok`
--

INSERT INTO `kelompok` (`id`, `nama_kelompok`) VALUES
(1, 'Kelompok 1'),
(2, 'Kelompok 2'),
(3, 'Kelompok 3'),
(4, 'Kelompok 4');

-- --------------------------------------------------------

--
-- Table structure for table `kordinator`
--

CREATE TABLE `kordinator` (
  `id` int(11) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kordinator`
--

INSERT INTO `kordinator` (`id`, `nik`, `nama`) VALUES
(1, '180909', 'kordinator'),
(2, '08096544', 'kordinator proyek 1'),
(3, '08096544', 'kordinator proyek 2');

-- --------------------------------------------------------

--
-- Table structure for table `logbook`
--

CREATE TABLE `logbook` (
  `id` int(11) NOT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_kelompok` int(11) DEFAULT NULL,
  `id_mhs` int(11) DEFAULT NULL,
  `id_proposal` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` varchar(100) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `time` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `id` int(11) NOT NULL,
  `nim` varchar(10) DEFAULT NULL,
  `nama` varchar(100) NOT NULL,
  `id_semester` int(11) DEFAULT NULL,
  `tahun` char(4) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`id`, `nim`, `nama`, `id_semester`, `tahun`) VALUES
(1, '1603030', 'ambar', 1, '2019'),
(2, '1603030', 'ijang', 1, '2019'),
(3, '1603030', 'amel', 1, '2019'),
(4, '1603030', 'samsul', 1, '2019'),
(5, '160', 'adehilmi', NULL, ''),
(6, '160', 'uciramadhani', NULL, ''),
(7, '160', 'fahmi muhammad', NULL, ''),
(8, '160', 'aditya', NULL, ''),
(9, '1603053', 'dhandi', 3, '2018'),
(10, '16030', 'rizkhan', NULL, ''),
(11, '16030', 'nezla', NULL, ''),
(12, '16030', 'atun', NULL, ''),
(13, '16030', 'atun', NULL, ''),
(14, '16030', 'atun', NULL, ''),
(15, '16030', 'atun', NULL, ''),
(16, '16030', 'atun', NULL, ''),
(17, '16030', 'atun', NULL, ''),
(18, '16030', 'atun', NULL, ''),
(19, '16030', 'neta', NULL, ''),
(20, '16030', 'neta', NULL, ''),
(21, '16030', 'rifaldi', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `pengampu`
--

CREATE TABLE `pengampu` (
  `id` int(11) NOT NULL,
  `nik` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengampu`
--

INSERT INTO `pengampu` (`id`, `nik`, `nama`) VALUES
(1, '16039001', 'lukman sifa'),
(2, '16039002', 'munengsih sari bunga');

-- --------------------------------------------------------

--
-- Table structure for table `proposal`
--

CREATE TABLE `proposal` (
  `id` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `id_kelompok` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `file` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `time` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proposal`
--

INSERT INTO `proposal` (`id`, `id_mhs`, `id_kelompok`, `id_kelas`, `judul`, `file`, `status`, `time`) VALUES
(1, 2, 2, 10, 'Proposal Rizaldi', '1567065791_TI KJM Ganjil 2018-2019.xlsx', 1, '2019-01-01'),
(2, 2, 2, 10, 'Proposal Rizaldi 2', '1567065911_datadosen.xlsx', 1, '2019-01-01'),
(3, 4, 2, 10, 'Proposal Samsul', '1567065949_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01'),
(4, 1, 1, 10, 'Proposal Ambar', '1567066076_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01'),
(5, 8, 2, 9, 'Proposal Aditya', '1567066272_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01'),
(6, 8, 2, 9, 'Proposal Aditya 2', '1567066520_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01'),
(7, 6, 2, 9, 'Proposal Uci', '1567066784_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01'),
(8, 1, 1, 10, 'Proposal Pengajuan Dana', '1568617356_TI KJM Ganjil 2018-2019.xlsx', 2, '2019-09-16'),
(9, 1, 1, 10, 'Proposal Kelompok 1', '1568637138_KOMUNIKASI BISNIS.docx', 1, '2019-09-16');

-- --------------------------------------------------------

--
-- Table structure for table `revisi`
--

CREATE TABLE `revisi` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `id_kelompok` int(11) NOT NULL,
  `id_proposal` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mhs` int(11) NOT NULL,
  `file` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id`, `nama`) VALUES
(1, 'Semester I'),
(2, 'Semester II'),
(3, 'Semester III'),
(4, 'Semester IV'),
(5, 'Semester V'),
(6, 'Semester VI');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_kordinator` int(11) DEFAULT NULL,
  `id_pengampu` int(11) DEFAULT NULL,
  `id_mahasiswa` int(11) DEFAULT NULL,
  `id_user_role` int(11) DEFAULT NULL,
  `id_kelas` int(11) DEFAULT NULL,
  `id_kelompok` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `token` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `id_kordinator`, `id_pengampu`, `id_mahasiswa`, `id_user_role`, `id_kelas`, `id_kelompok`, `status`, `token`) VALUES
(1, 'nugie', '$2y$13$MWTQljb2XlBTZI3OPx0NzOSfP4FGakwgjIrxtRTO02RlMwe1wFuKu', 0, 0, 0, 1, 0, 0, 1, '9E4V9_8F6vqzq-fMP0aFCxpygESXqbJ72pJHJC2CpE6xsyIrJvfzLw-0LWiipFnsE83Tx-KBKP6JunEfciYx6ZtFRuEmt0jlNSYe'),
(2, 'sams', '$2y$13$MWTQljb2XlBTZI3OPx0NzOSfP4FGakwgjIrxtRTO02RlMwe1wFuKu', 1, 0, 0, 3, 0, 0, 1, '9E4V9_8F6vqzq-fMP0aFCxpygESXqbJ72pJHJC2CpE6xsyIrJvfzLw-0LWiipFnsE83Tx-KBKP6JunEfciYx6ZtFRuEmt0jlNSYe'),
(3, 'ambar', '$2y$13$8QUz7iNPTDi4.rrv.1JlHuO.RrhALADKgDxBBwKHLgNQ5465Oidv6', 0, 0, 1, 2, 10, 1, 1, '97RT49_vnSynIqDsBbiSAXwfKcRNlyO96reHusm3w2YtoPz8_dunf5GYqX66ba4FMmvABpK86L2hQnPXmKZv5TNDikjN8NLjS2zI'),
(4, 'rizaldi', '$2y$13$bxLSMqtLmDwUyVE3Dko7WOo.n6mR606kcN7UhkqOCMLlI1tts7/ei', 0, 0, 2, 2, 10, 2, 1, 'rwx8ff5ihb942dSiNkwK2CUf0cjKM4LfpKTjpklPYUa7QcCndSgBKbD05B6eiZmWZtgpH-90o7pO1wak2bBpVX3kxABzIsRyuf9Y'),
(5, 'amelia', '$2y$13$gg5C6HFXU7LevK0QgF1RpuKv6.vZCCqGs9Rn28tT.hEda1f4FGi4G', 0, 0, 3, 2, 10, 1, 1, 'kEwk0Xax8uMgHEdvhO6-FmEGEpLkPU9B-ZjDqezqWobzKvNJzc5_NQ6DGpCnvZ_R1_J3tXle7gEKaC_rQONm_-8C0sXxHEumB9u0'),
(6, 'samsul', '$2y$13$/tCPD8P91otJzdUjwExJVOmuXC6ex8X38PK/UG9CGeW/5wymVfBcG', 0, 0, 4, 2, 10, 2, 1, 'vEwZmTA7a-fahY5be5nJKXVsuNc3FmoAucCDkMxPxG_yl49SVKm5tHgvIie_SFantjkgCazwhB6b55sKTpHT-KVwghcHwoMWNZ3D'),
(7, 'palukman', '$2y$13$1GYmIr5apIddHYCufi/WL.1kuIZyiWXZTxETqbrE5JKjhDMGMhVKq', 0, 1, 0, 4, 10, 0, 1, 'Df6_bcWc-J4WO4cDquERBP57Tz5Y-clXJM7EHMUzl0WDKjBnsZYDzIVnALhGh-Bcz5UpblTuHzSOUXq4Fso6TX4FoyT5hBUfTqMS'),
(8, 'adehilmi', '$2y$13$I4lPVaVUdA2xT29opOHitOCml68dFecdv5KwWNpP20Il9gJIGyeyu', 0, 0, 5, 2, 9, 1, 1, 'xBAtmYUKqFnpMSHqASu7SyHTzzBpVupvI8gDPrhlXf80dT8tN0AMpQ2l_A--qeg2-x9H0eflMzNbm01kkKhw3wNBPhkWpiPPo-3H'),
(9, 'uci', '$2y$13$4kl5IXCuWMYxcaA/.OzoF.1zdZxeUAWGTfjikgc65Unrj1mItNpF2', 0, 0, 6, 2, 9, 2, 1, 'ljFWrHf86u75utqAROi6rVdThDAkXrkI_4JX7jwnAsrfQH8T5G1r8DQHf46uSg3AQCf_St1mgRio0Na2fFCXZwBlchf2wH4DK3Xw'),
(10, 'fahmi', '$2y$13$mJ3AtkP.o90aLPKBrV1KOe4Q5cIMyaSYJEKWS/sgOheGTToQFWVWy', 0, 0, 7, 2, 9, 1, 1, 'krJAVi-FKllJ0SX7rEhx9EzYwNLycRCQ-2uu7PUsgSyMkXKc7VTsj9SwC-QWQqkA6Uq20Ty44fTd1aNdP0CqaSAoMAqXe2hcdgcs'),
(11, 'aditya', '$2y$13$2dZYfOWdkYVvjyPDQfBSyudtuCumImRguHnkTKj5FhbHtAkD1mwn6', 0, 0, 8, 2, 9, 2, 1, 'epUyrhwpfJP_YoJV6DFgDjoHOoKdBI1jnijgJo4ZyYVLm4A24qqlyeFhIQDHyIUQgIssjnaqcBOuPrxlIzcY1zclHS6aOQVfGduE'),
(12, 'bumun', '$2y$13$2dZYfOWdkYVvjyPDQfBSyudtuCumImRguHnkTKj5FhbHtAkD1mwn6', 0, 2, 0, 4, 9, 0, 1, 'epUyrhwpfJP_YoJV6DFgDjoHOoKdBI1jnijgJo4ZyYVLm4A24qqlyeFhIQDHyIUQgIssjnaqcBOuPrxlIzcY1zclHS6aOQVfGduE'),
(13, 'dhandi', '$2y$13$R.efyUziN6aVfNjKffxrj.vB9.7B0pUHB5t1zoMLOe03hHdXdNkYe', 0, 0, 9, 2, 10, 1, 1, 'NjzdgVMira-mgct3EAep5uGgPO9p_HL4MH3ioXbR6Og3__X40wzBXVq2dKx88sanf8boN-kELUuJfVLHYD_AjU0nB-wzB_zHVdWg'),
(14, 'rizkhan', '$2y$13$gqyfkiSNK2naEzvGyFnRVuNSTMLIMOqkfzvvB39smCjSK.ulR6Giu', 0, 0, 10, 2, 10, 1, 1, 'Q2uCRlIRtfNLxYBtk7AI29dmtoPPUM5DtkQFOACZRjsV0q2OMF3gCLgOYc5hHlz88edp5YqGUUUwRljqhsTU9MNCNrhFvD3QHcrc'),
(15, 'nezla', '$2y$13$ohh0RCFSjeGrs1lQSgp7leb20dn0AoGAbjzFnKAetBO3ClgRogCDi', 0, 0, 11, 2, 11, 11, 1, 'yaB5JWYVo72QHz57VUF3ndGj6lRKD_tLHRXin_7peJoZJrOOqOzd9LqNdb7DwrysSLEEhuTxXhdGyTPcmDHnkSVtd-HLITL0b_iR'),
(16, 'atun', '$2y$13$ayQ5ZThGciF71bcp3Jmoku/AZCqYN/7rxADx3AzbiGZAsY2.ILjNy', 0, 0, 18, 2, 10, 2, 1, 'fyGiBmKtNCjcOogutn_hKsPQbv41Vd8WaQvpd7VLhA5pzyeuUIkyH9c2zwxTLaUvG7oLaDj_FRJuWmaGduHBxqhK37lRNlcY-Ud-'),
(17, 'neta', '$2y$13$15ls8Yi8OFTssBWi9GjCGOTvSWoeKsJTxrMQV6FfVT8EHswu6Eus6', 0, 0, 20, 2, 2, 1, 1, 'mePhR2bsoWDYcpZk3DnWy56H7h9uRTzulmeD3TI2ZoznBw9RQftp562grHCeS0iIy1ItcvYchLCH8dZInXFlrCJ-Ehe0woKQLojU'),
(18, 'rifal', '$2y$13$zKssfxjoXW4K96SwUuQ1FuqbL6M7XhRD2NaaT3rQ83soodxFbJEhK', 0, 0, 21, 2, 10, 2, 1, 'iSuQHtiwomvyjsdoIUmcZViOcJgfCUFnGnsmHrUIbX_powRb13uK0Ox2mVTRus7vSpw0f3VHBDE8D2JUrBERNiOxML07NKPypzpc'),
(19, 'kordinator1', '$2y$13$PsXXk07RnCFX6okRnDws.OBlaNa/7QrpPYSolmYVSs3sYvNltM9kK', 2, 0, 0, 3, 2, 2, 1, 'F-HgeQEssDhPrfFo7xcrxd4K2mm14W-jSeq6OfKlnJM6O4JD-D-0CzxlbJ71cIZ4cBYc-ifD8ZakxApwsWWNe_In9MVK-Mudbr3x'),
(20, 'kordinator2', '$2y$13$hQg1CkDgIl56581Jprq0j.A8x.DSH00nZdaXwAdTM.3i3VWS9K8yu', 3, 0, 0, 3, 3, 3, 1, 'FfYMVCY3kkoHsHoVE6IOZFrx5Y9Oc4hCE7G2WoX9GKHNToiKWP4o-vVlDwa0nMvb4fdMKoYaa1qD4FgoP_OXA72lU07sYQAqadh3');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `nama`) VALUES
(1, 'admin'),
(2, 'mahasiswa'),
(3, 'kordinator'),
(4, 'pengampu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `informasi`
--
ALTER TABLE `informasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelompok`
--
ALTER TABLE `kelompok`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kordinator`
--
ALTER TABLE `kordinator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logbook`
--
ALTER TABLE `logbook`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengampu`
--
ALTER TABLE `pengampu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proposal`
--
ALTER TABLE `proposal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `revisi`
--
ALTER TABLE `revisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `informasi`
--
ALTER TABLE `informasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `kelompok`
--
ALTER TABLE `kelompok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kordinator`
--
ALTER TABLE `kordinator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `logbook`
--
ALTER TABLE `logbook`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `pengampu`
--
ALTER TABLE `pengampu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `proposal`
--
ALTER TABLE `proposal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `revisi`
--
ALTER TABLE `revisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
