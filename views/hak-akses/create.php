<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Models\HakAkses */

$this->title = 'Create Hak Akses';
$this->params['breadcrumbs'][] = ['label' => 'Hak Akses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hak-akses-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
