<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\HakAkses */

$this->title = 'Update Hak Akses: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Hak Akses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="hak-akses-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
