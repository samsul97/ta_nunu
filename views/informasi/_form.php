<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\file\FileInput;
// use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Informasi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="informasi-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'tempat')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'image_upload')->widget(FileInput::classname(),
            [
                        // 'data' => $model->file,
                'options' => ['multiple' => false],
            ]);
            ?>

            <div class="box-footer">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>