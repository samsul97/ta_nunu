<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Informasi */

$this->title = 'Tambah Informasi';
$this->params['breadcrumbs'][] = ['label' => 'Informasi', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="informasi-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
