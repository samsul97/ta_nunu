<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\InformasiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Informasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app ->user->identity->id_user_role==1):?>
    <div class="informasi-index box box-primary">
        <!-- <div class="box-header with-border">
            <?= Html::a('Create Informasi', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
        </div> -->
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                 [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'headerOptions' => ['style' => 'text-align:center'],
                    'contentOptions' => ['style' => 'text-align:center']
                ],
               // 'id',
                'judul',
                // 'isi',
                'tanggal',
                'keterangan:ntext',
                'tempat',
                // 'tempat',
                [
                    'attribute' => 'image',
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'text-align:center'],
                    'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                    'value' => function ($model)
                    {
                        if ($model->image !== '') {
                            return Html::img('@web/upload/' . $model->image, ['style'=>'height:80px', 'width:100px;']);
                        }
                        else{
                            return '<div align="center"><h1>File tidak ada</h1></div>';
                        }
                    },   
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'text-align:center;width:80px']
                ],
            ],
        ]); ?>
    </div>
</div>
<?php endif ?>
<?php if (Yii::$app ->user->identity->id_user_role==2):?>
    <div class="informasi-index box box-primary">
        <div class="box-header with-border">
            
        </div>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                 [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'headerOptions' => ['style' => 'text-align:center'],
                    'contentOptions' => ['style' => 'text-align:center']
                ],
               // 'id',
                'judul',
                // 'isi',
                'tanggal',
                'keterangan:ntext',
                'tempat',
                // 'File',
                [
                    'attribute' => 'image',
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'text-align:center'],
                    'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                    'value' => function ($model)
                    {
                        if ($model->image !== '') {
                            return Html::img('@web/upload/' . $model->image, ['style'=>'height:80px', 'width:100px;']);
                        }
                        else{
                            return '<div align="center"><h1>File tidak ada</h1></div>';
                        }
                    },   
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=> '{view}',
                    'contentOptions' => ['style' => 'text-align:center;width:80px']
                ],
            ],
        ]); ?>
    </div>
</div>
<?php endif ?>
<?php if (Yii::$app ->user->identity->id_user_role==3):?>
    <div class="informasi-index box box-primary">
        <div class="box-header with-border">
            <?= Html::a('Create Informasi', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
        </div>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                 [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'headerOptions' => ['style' => 'text-align:center'],
                    'contentOptions' => ['style' => 'text-align:center']
                ],
               // 'id',
                'judul',
                // 'isi',
                'tanggal',
                'keterangan:ntext',
                'tempat',
                // 'tempat',
                [
                    'attribute' => 'image',
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'text-align:center'],
                    'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                    'value' => function ($model)
                    {
                        if ($model->image !== '') {
                            return Html::img('@web/upload/' . $model->image, ['style'=>'height:80px', 'width:100px;']);
                        }
                        else{
                            return '<div align="center"><h1>File tidak ada</h1></div>';
                        }
                    },   
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'text-align:center;width:80px']
                ],
            ],
        ]); ?>
    </div>
</div>
<?php endif ?>