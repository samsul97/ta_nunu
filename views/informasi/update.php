<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Informasi */

$this->title = 'Edit : ' . $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Informasis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="informasi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
