<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Informasi */

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Informasis', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app ->user->identity->id_user_role==1):?>
    <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                'judul',
                // 'isi',
                'tanggal',
                'keterangan:ntext',
                'tempat',
            ],
        ]) ?>
    </div>
</div>
<?php endif ?>
<div class="informasi-view box box-primary">

    <?php if (Yii::$app ->user->identity->id_user_role==2):?>
        
        <div class="box-body table-responsive no-padding">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                // 'id',
                    'judul',
                // 'isi',
                    'tanggal',
                    'keterangan:ntext',
                    'tempat',
                ],
            ]) ?>
        </div>
    </div>
<?php endif ?>
<?php if (Yii::$app ->user->identity->id_user_role==3):?>
    <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                'judul',
                // 'isi',
                'tanggal',
                'keterangan:ntext',
                'tempat',
            ],
        ]) ?>
    </div>
</div>
<?php endif ?>