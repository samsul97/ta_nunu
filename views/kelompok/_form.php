<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use kartik\select2\Select2;
use app\models\Kelas;
use app\models\Mahasiswa;

/* @var $this yii\web\View */
/* @var $model app\models\Kelompok */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kelompok-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        
        <?= $form->field($model, 'nama_kelompok')->textInput(['maxlength' => true]) ?>
        
        </div>
        <div class="box-footer">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
