<?php

use app\models\User;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KelompokSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kelompok';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelompok-index box box-primary">
    <div class="box-header with-border">
        <?php if ((Yii::$app->user->identity->id_user_role === 1) || (Yii::$app->user->identity->id_user_role === 3) || (User::isKordinator())): ?>
            <?= Html::a('Create Kelompok', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
        <?php endif ?>

        </div>

    <div class="box-body table-responsive no-padding">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'headerOptions' => ['style' => 'text-align:center'],
                    'contentOptions' => ['style' => 'text-align:center']
                ],
                'nama_kelompok',

                [
                    'template' => Yii::$app->user->identity->id_user_role === 2 ? '{view}' : '{view}{update}{delete}',
                    'class' => ActionColumn::class,
                    'contentOptions' => ['style' => 'text-align:center;width:80px']
                ],
            ],
        ]) ?>
    </div>
</div>