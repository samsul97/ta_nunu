<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Kelompok;
use app\models\Kelas;
$this->title = 'Random Kelompok';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelompok-index box box-primary">
	<div class="box-header with-border">	
		<div class="card" style="width: 18rem;">
			<div class="card-body">
				<h5 class="card-title">Kelas D3TI2B</h5>
			</div>
		</div>
		<?= Html::a('Generate', ['random-kelompok'], ['class' => 'btn btn-info btn-flat']) ?>
	</div>
</div>