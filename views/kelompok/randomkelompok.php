<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Kelompok;

$this->title = 'Hasil Random Kelompok';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kelompok-index box box-primary">
	<div class="box-header with-border">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Hasil Random Kelompok</h3>
			</div>
			<div class="box-body">
				<table class="table table-bordered table-hover">
					<thead class="bg-blue">
						<tr>
							<th style="text-align: center; color: black; width: 50px;">No</th>
							<th style="text-align: center; color: black;">Nama Kelompok</th>
						</tr>
					</thead>
					<?php
					$mahasiswa = \app\models\Mahasiswa::find()->asArray()->all();
					$jml_kelompok = 2;
					$perkelompok  = count($mahasiswa) / $jml_kelompok;
					$mahasiswa_gaib[] = [
						'id' => 0,
						'nim' => '-',
						'nama' => '-',
						'id_kelas' => '-'
					];
					$kelompok = Kelompok::find()->all();
					foreach ($kelompok as $kel)
					{
						echo '<tr><th colspan="3" class="info"> ' .
						$kel->nama_kelompok .
						"</th></tr>";

						$i = 1;
						if (count($mahasiswa) % 2 !== 0) { 
							$mahasiswa = array_merge($mahasiswa,$mahasiswa_gaib);
						} 
						for ($i = 0; $i < $jml_kelompok; $i++)
						{
							$kel = array_rand($mahasiswa, round($perkelompok));
							$kelompok = 'Kelompok ' . ($i + 1);
							foreach ($kel as $key => $value) {
								// if ($kelompok->id_mahasiswa === $mahasiswa->id) {
								echo '<tr>
								<td style="text-align: center;">' . $i++ . '</td>
								<td>' . sprintf("insert %s %s", $kelompok, $mahasiswa[$value]['nama']) . '</td>
								</tr>';
							}
						}
					}
					?>
				</table>
			</div>
		</div>
	</div>
</div>