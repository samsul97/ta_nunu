<?php

use app\models\Kelompok;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kelompok */

$this->title = $model->nama_kelompok;
$this->params['breadcrumbs'][] = ['label' => 'Kelompok', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="kelompok-view box box-primary">
        <div class="box-header">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger btn-flat',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="box-body table-responsive no-padding">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    'nama_kelompok',
                ],
            ]) ?>
        </div>
    </div>

<?php if (Yii::$app->user->identity->id_user_role !== 1): ?>
    <div>&nbsp;</div>
    <div class="box box-primary">
        <div class="box-body">
            <div class="box-header">
                <h3 class="box-title">Daftar Mahasiswa</h3>
            </div>
            <div>&nbsp;</div>
            <table class="table">
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <?php if (Yii::$app->user->identity->id_user_role === 3) : ?>
                        <th>Kelas</th>
                    <?php endif; ?>
                    <th></th>
                </tr>
                <?php $no = 1;
                foreach (Kelompok::findAllKelompok($model->id) as $mahasiswa): ?>
                    <tr>
                        <td><?= $no; ?></td>
                        <td><?= Html::a($mahasiswa->nama, ['mahasiswa/view', 'id' => $mahasiswa->id]); ?></td>
                        <?php if (Yii::$app->user->identity->id_user_role === 3) : ?>
                            <td><?= Html::a($mahasiswa->kelas->nama, ['kelas/view', 'id' => $mahasiswa->user->id]) ?></td>
                        <?php endif; ?>
                        <td>
                            <?= Html::a("<i class='fa fa-pencil'> Edit</i>", ["mahasiswa/update", "id" => $mahasiswa->id], ['class' => 'btn btn-primary']) ?>
                            &nbsp;
                            <?= Html::a("<i class='fa fa-trash'> Hapus</i>", ["mahasiswa/delete", "id" => $mahasiswa->id], ['class' => 'btn btn-danger', 'data-method' => 'post', 'data-confirm' => 'Yakin hapus data ini?']) ?>
                            &nbsp;
                        </td>
                    </tr>
                    <?php $no++; endforeach ?>
            </table>
        </div>
    </div>
<?php endif ?>


<?php if (Yii::$app->user->identity->id_user_role === 1): ?>
    <p>
        <?= Html::a('Tambah Mahasiswa', ['semester-mahasiswa/create', 'kelompokId'=>$model->id], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'semesterCalender.semester',
            'kelompok.nama',
            'mahasiswa.nama',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php endif ?>
