<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Kelas;
use app\models\Kelompok;
use app\models\Proposal;
use app\models\Semester;
use app\models\Mahasiswa;
use kartik\file\FileInput; 

/* @var $this yii\web\View */
/* @var $model app\models\Logbook */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logbook-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

       <?= $form->field($model, 'id_proposal')->widget(Select2::classname(),
        [
            'data' => Proposal::getList(),
            'options' => [
                'placeholder' => '-Pilih Proposal-',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],

        ]);
        ?>

        <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'isi')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'keterangan')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
