<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PostLogbook */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="logbook-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_kelas') ?>

    <?= $form->field($model, 'id_kelompok') ?>

    <?= $form->field($model, 'id_proposal') ?>

    <?= $form->field($model, 'id_mhs') ?>

    <?php // echo $form->field($model, 'judul') ?>

    <?php // echo $form->field($model, 'isi') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <?php // echo $form->field($model, 'time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
