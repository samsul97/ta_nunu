<?php

use app\models\Mahasiswa;
use app\models\SemesterCalender;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Logbook */

$this->title = 'Export Logbook';
$this->params['breadcrumbs'][] = ['label' => 'Logbooks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="logbook-create">

    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'class' => 'form-horizontal',
    ]); ?>
    <?= $form->field($model, 'mahasiswa_id')->widget(Select2::class,
        [
            'data' => Mahasiswa::getList(),
            'options' => [
                'placeholder' => '-Pilih Mahasiswa-',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
    ?>

    <?= $form->field($model, 'semester_kalender_id')->widget(Select2::class,
        [
            'data' => SemesterCalender::getList(),
            'options' => [
                'placeholder' => '-Pilih Semester-',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
    ?>
    <div class="box-footer">
        <?= Html::submitButton('Export', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
