<?php

use app\models\Kelas;
use app\models\Kelompok;
use app\models\Mahasiswa;
use app\models\Proposal;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostLogbook */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logbooks';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->user->identity->id_user_role == 1): ?>
    <div class="logbook-index box box-primary">
        <!-- <div class="box-header with-border">
            <?= Html::a('Create Logbook', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
        </div> -->
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelas->nama;
                        }
                    ],

                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],

                    [
                        'attribute' => 'id_proposal',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Proposal::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->proposal->judul;
                        }
                    ],
                    'judul',
                    'isi',
                    'keterangan',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (Yii::$app->user->identity->id_user_role == 2): ?>
    <div class="logbook-index box box-primary">
        <div class="box-header with-border">
            <?= Html::a('Create Logbook', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
            <?= Html::a('Export', ['logbook/export'], ['class' => 'btn btn-info btn-flat']) ?>
        </div>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelas->nama;
                        }
                    ],

                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],

                    [
                        'attribute' => 'id_proposal',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Proposal::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->proposal->judul;
                        }
                    ],
                    'judul',
                    'isi',
                    'keterangan',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (Yii::$app->user->identity->id_user_role == 3): ?>
    <div class="logbook-index box box-primary">
        <div class="box-header with-border">

        </div>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],

                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelas->nama;
                        }
                    ],

                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],

                    [
                        'attribute' => 'id_proposal',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Proposal::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->proposal->judul;
                        }
                    ],

                    'judul',
                    'isi',
                    'keterangan',
                    [
                        'template' => '{view}',
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>