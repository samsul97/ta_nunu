<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Logbook */

$this->title = 'Update Logbook: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Logbooks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="logbook-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
