<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DataBdt */

$this->title = 'Import Data Mahasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Import Data', 'url' => ['index']];
?>

<!-- <h2>Import Data</h2> -->
<div class="box box-solid box-info">
    <div class="box-header">
        <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
        <div class="box-tools pull-right">
		</div>
	</div>
</div>
<div class="page-content">  
     <div class="box box-primary">        
        <div class="box-body">
        	<?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?> 
			<?= $form->field($modelImport, 'fileImport')->fileInput() ?> 
			<?= Html::submitButton('Import', ['class'=>'btn btn-primary']); ?> 
			<?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

