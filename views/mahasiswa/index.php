<?php

use app\models\Kelas;
use app\models\Kelompok;
use app\models\Mahasiswa;
use app\models\Semester;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\webs\View */
/* @var $searchModel app\models\MahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->user->identity->id_user_role == 1): ?>
    <div class="mahasiswa-index box box-primary">
        <div class="box-header with-border">
            <p style="float: right">
                <?= Html::a('Import', ['import'], ['class' => 'btn btn-info']) ?>
            </p>
        </div>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

                    'nim',
                    'nama',

                    [
                        'attribute' => 'id_semester',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Semester::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function (Mahasiswa $data) {
                            if ($data->getCurrentSemesterData()->one() === null) {
                                return '';
                            }

                            if ($data->getCurrentSemesterData()->one()->getSemesterCalender()->one() === null) {
                                return '';
                            }
                            return $data->getCurrentSemesterData()->one()->getSemesterCalender()->one()->semester;
                        }
                    ],
                    'tahun',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (Yii::$app->user->identity->id_user_role == 2): ?>
    <div class="mahasiswa-index box box-primary">
        <!-- <div class="box-header with-border">
            <?= Html::a('Create Mahasiswa', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
        </div> -->
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

                    // 'id',
                    'nim',
                    'nama',
                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelas->nama;
                        }
                    ],

                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama;
                        }
                    ],

                    [
                        'attribute' => 'id_semester',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Semester::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->semester->nama;
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (Yii::$app->user->identity->id_user_role == 3): ?>
    <div class="mahasiswa-index box box-primary">
        <!-- <div class="box-header with-border">
            <?= Html::a('Create Mahasiswa', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
        </div> -->
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

                    // 'id',
                    'nim',
                    'nama',
                    // [
                    //     'attribute' => 'id_kelas',
                    //     'format' => 'raw',
                    //     'headerOptions' => ['style' => 'text-align:center'],
                    //     'filter' => Kelas::getList(),
                    //     'contentOptions' => ['style' =>'text-align:center;'],
                    //     'value' => function ($data) {
                    //         return @$data->kelas->nama;
                    //     }
                    // ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>