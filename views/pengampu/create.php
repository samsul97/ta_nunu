<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\Models\Pengampu */

$this->title = 'Create Pengampu';
$this->params['breadcrumbs'][] = ['label' => 'Pengampus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengampu-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
