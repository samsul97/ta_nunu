<?php
use app\models\Kelas;
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PengampuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengampu';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app ->user->identity->id_user_role==1):?>
    <div class="pengampu-index box box-primary">
        <div class="box-header with-border">
            
        </div>

        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],
                    'nik',
                    'nama',
                // [
                //     'attribute' => 'id_kelas',
                //     'format' => 'raw',
                //     'headerOptions' => ['style' => 'text-align:center'],
                //     'filter' => Kelas::getList(),
                //     'contentOptions' => ['style' =>'text-align:center;'],
                //     'value' => function ($data) {
                //         return @$data->kelas->nama;
                //     }
                // ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (Yii::$app ->user->identity->id_user_role==2):?>
    <div class="pengampu-index box box-primary">
    <!-- <div class="box-header with-border">
        <?= Html::a('Create Pengampu', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
    </div> -->
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'headerOptions' => ['style' => 'text-align:center'],
                    'contentOptions' => ['style' => 'text-align:center']
                ],
                'nik',
                'nama',
                // [
                //     'attribute' => 'id_kelas',
                //     'format' => 'raw',
                //     'headerOptions' => ['style' => 'text-align:center'],
                //     'filter' => Kelas::getList(),
                //     'contentOptions' => ['style' =>'text-align:center;'],
                //     'value' => function ($data) {
                //         return @$data->kelas->nama;
                //     }
                // ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'text-align:center;width:80px']
                ],
            ],
        ]); ?>
    </div>
</div>
<?php endif ?>
<?php if (Yii::$app ->user->identity->id_user_role==3):?>
    <div class="pengampu-index box box-primary">
    <!-- <div class="box-header with-border">
        <?= Html::a('Create Pengampu', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
    </div> -->
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'header' => 'No',
                    'headerOptions' => ['style' => 'text-align:center'],
                    'contentOptions' => ['style' => 'text-align:center']
                ],
                'nik',
                'nama',
                // [
                //     'attribute' => 'id_kelas',
                //     'format' => 'raw',
                //     'headerOptions' => ['style' => 'text-align:center'],
                //     'filter' => Kelas::getList(),
                //     'contentOptions' => ['style' =>'text-align:center;'],
                //     'value' => function ($data) {
                //         return @$data->kelas->nama;
                //     }
                // ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'contentOptions' => ['style' => 'text-align:center;width:80px']
                ],
            ],
        ]); ?>
    </div>
</div>
<?php endif ?>