<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Pengampu */

$this->title = 'Update Pengampu: ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Pengampus', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
// $this->params['breadcrumbs'][] = 'Update';
?>
<div class="pengampu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
