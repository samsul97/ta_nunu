<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Mahasiswa;
use app\models\Kelompok;
use app\models\Kelas;
use app\models\Judul;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model app\models\Proposal */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if (Yii::$app->user->identity->id_user_role == 1): ?>
    <div class="proposal-form box box-primary">
        <?php $form = ActiveForm::begin(); ?>
        <div class="box-body table-responsive">
            <?= $form->field($model, 'id_mhs')->widget(Select2::classname(),
                [
                    'data' => Mahasiswa::getList(),
                    'options' => [
                        'placeholder' => '-Pilih NIM-',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],

                ]);
                ?>
                <?= $form->field($model, 'id_kelompok')->widget(Select2::classname(),
                    [
                        'data' => Kelompok::getList(),
                        'options' => [
                            'placeholder' => '-Pilih Kelompok-',
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],

                    ]);
                    ?>

                    <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),
                        [
                            'data' => Kelas::getList(),
                            'options' => [
                                'placeholder' => '-Pilih Kelas-',
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],

                        ]);
                        ?>

                        <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'file_upload')->widget(FileInput::classname(),
                            [
                                'data' => $model->file,
                                'options' => ['multiple' => false],
                            ]);
                            ?>        

                        </div>
                        <div class="box-footer">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                <?php endif ?>

                <?php if (Yii::$app->user->identity->id_user_role == 2): ?>
                    <div class="proposal-form box box-primary">
                        <?php $form = ActiveForm::begin(); ?>
                        <div class="box-body table-responsive">
                            <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'file_upload')->widget(FileInput::classname(),
                                [
                                    'data' => $model->file,
                                    'options' => ['multiple' => false],
                                ]);
                                ?>
                            </div>
                            <div class="box-footer">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    <?php endif ?>

                    
                    <?php if (Yii::$app->user->identity->id_user_role == 3): ?>
                        <div class="proposal-form box box-primary">
                            <?php $form = ActiveForm::begin(); ?>
                            <div class="box-body table-responsive">

                                <?= $form->field($model, 'id_mhs')->widget(Select2::classname(),
                                    [
                                        'data' => Mahasiswa::getList(),
                                        'options' => [
                                            'placeholder' => '-Pilih NIM-',
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],

                                    ]);
                                    ?>

                                    <?= $form->field($model, 'id_kelompok')->widget(Select2::classname(),
                                        [
                                            'data' => Kelompok::getList(),
                                            'options' => [
                                                'placeholder' => '-Pilih Kelompok-',
                                            ],
                                            'pluginOptions' => [
                                                'allowClear' => true
                                            ],

                                        ]);
                                        ?>

                                        <?= $form->field($model, 'id_kelas')->widget(Select2::classname(),
                                            [
                                                'data' => Kelas::getList(),
                                                'options' => [
                                                    'placeholder' => '-Pilih Kelas-',
                                                ],
                                                'pluginOptions' => [
                                                    'allowClear' => true
                                                ],

                                            ]);
                                            ?>

                                            <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

                                            <?= $form->field($model, 'file_upload')->widget(FileInput::classname(),
                                                [
                                                    'data' => $model->file,
                                                    'options' => ['multiple' => false],
                                                ]);
                                                ?>        

                                            </div>
                                            <div class="box-footer">
                                                <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
                                            </div>
                                            <?php ActiveForm::end(); ?>
                                        </div>
                                        <?php endif ?>