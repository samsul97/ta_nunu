<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Proposal */

$this->title = 'Tambah Proposal';
$this->params['breadcrumbs'][] = ['label' => 'Proposals', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
