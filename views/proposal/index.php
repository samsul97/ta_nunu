<?php

use app\models\Kelas;
use app\models\Kelompok;
use app\models\Mahasiswa;
use app\models\SemesterCalender;
use app\models\User;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Proposal */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proposal';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (User::isAdmin()): ?>
    <div class="proposal-index box box-primary">
        <!-- <div class="box-header with-border">
                <?= Html::a('Create Proposal', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
            </div> -->
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

                    // 'id',
                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->getKelas()->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],
                    'judul',

                    [
                        'attribute' => 'file',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align:center; width:80px'],
                        'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                        'value' => function ($model) {
                            if ($model->file !== '') {
                                return '<a href="' . Yii::$app->request->baseUrl . '/upload/' . $model->file . '"><div align="center"><button class="btn btn-success glyphicon glyphicon-download-alt" type="submit"></button></div></a>';
                            } else {
                                return '<div align="center"><h1>File tidak ada</h1></div>';
                            }

                        },
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (User::isMahasiswa()): ?>
    <div class="proposal-index box box-primary">
        <?php if ($couldCreateNew) { ?>
            <div class="box-header with-border">
                <?= Html::a('Create Proposal', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
            </div>
        <?php } ?>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

                    // 'id',
                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelas->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],
                    'judul',
                    // 'file',

                    [
                        'attribute' => 'file',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align:center; width:80px'],
                        'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                        'value' => function ($model) {
                            if ($model->file !== '') {
                                return '<a href="' . Yii::$app->request->baseUrl . '/upload/' . $model->file . '"><div align="center"><button class="btn btn-success glyphicon glyphicon-download-alt" type="submit"></button></div></a>';
                            } else {
                                return '<div align="center"><h1>File tidak ada</h1></div>';
                            }

                        },
                    ],

                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            if ($model->status == '0') {
                                return '<button class ="btn btn-warning">' . "Belum Disetujui" . '</button>';
                            }
                            if ($model->status == '1') {
                                return '<button class ="btn btn-success">' . "Disetujui" . '</button>';
                            }
                            if ($model->status == '2') {
                                return '<button class ="btn btn-danger" onclick="alert(\'' . $model->declined_reason . '\')">' . "Tidak Disetujui" . '</button>';
                            }
                        },
                        'filter' => [
                            '0' => 'Belum Disetujui',
                            '1' => 'Disetujui',
                            '2' => 'Tidak Disetujui',

                        ],
                    ],

                    'time',

                    [
                        // 'template' => '{view}',
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (User::isPengampu() && !User::isKordinator()): ?>
    <div class="proposal-index box box-primary">
        <div class="box-header with-border">
        </div>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],


                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelas->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],
                    'judul',
                    [
                        'attribute' => 'file',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align:center; width:80px'],
                        'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                        'value' => function ($model) {
                            if ($model->file !== '') {
                                return '<a href="' . Yii::$app->request->baseUrl . '/upload/' . $model->file . '"><div align="center"><button class="btn btn-success glyphicon glyphicon-download-alt" type="submit"></button></div></a>';
                            } else {
                                return '<div align="center"><h1>File tidak ada</h1></div>';
                            }

                        },
                    ],


                    [
                        'template' => '{view}',
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (User::isKordinator()): ?>
    <div class="proposal-index box box-primary">
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

//                    'semesterMahasiswa.semesterCalender.tahun',
//                    'semesterMahasiswa.semesterCalender.semester'
                    [
                        'attribute' => 'tahun',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => SemesterCalender::getYearList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => static function ($data) {
                            return @$data->semesterMahasiswa->semesterCalender->tahun;
                        }
                    ],

                    [
                        'attribute' => 'semester',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => SemesterCalender::getSemesterList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => static function ($data) {
                            return @$data->semesterMahasiswa->semesterCalender->semester;
                        }
                    ],

                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelas->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],
                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' => 'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    'judul',
                    // 'file',

                    [
                        'attribute' => 'file',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align:center; width:80px'],
                        'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                        'value' => function ($model) {
                            if ($model->file !== '') {
                                return '<a href="' . Yii::$app->request->baseUrl . '/upload/' . $model->file . '"><div align="center"><button class="btn btn-success glyphicon glyphicon-download-alt" type="submit"></button></div></a>';
                            } else {
                                return '<div align="center"><h1>File tidak ada</h1></div>';
                            }

                        },
                    ],

                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a('<button class ="btn btn-' . ($model->status == 1 ? 'success' : 'danger') . '">' . ($model->status == 1 ? '' : 'Tidak ') . 'Disetujui </button>', ['proposal/edit-status', 'id' => $model->id, 'status' => ($model->status == 1 ? 2 : 1)], ['data' => ['confirm' => 'Apa Anda yakin ingin mengubah status ini?'],]);
                        },
                        'filter' => [

                            '1' => 'Disetujui',
                            '2' => 'Tidak Disetujui',
                        ],
                    ],

                    'time',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>