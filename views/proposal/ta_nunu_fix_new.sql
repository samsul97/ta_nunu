create table comments
(
    id          int auto_increment
        primary key,
    comment     varchar(100) not null,
    user_id     int          null,
    replying_to int          null,
    constraint comments_comments_id_fk
        foreign key (replying_to) references comments (id)
);

INSERT INTO ta_nunu_fix.comments (id, comment, user_id, replying_to) VALUES (1, 'Saya bingung kenapa kurikulumnya tidak mengacu pada kurikulum 2013?', null, null);
INSERT INTO ta_nunu_fix.comments (id, comment, user_id, replying_to) VALUES (2, 'Test', null, null);
create table informasi
(
    id         int auto_increment
        primary key,
    judul      varchar(100) not null,
    tanggal    date         not null,
    keterangan text         not null,
    tempat     varchar(100) not null,
    image      varchar(100) not null
);

INSERT INTO ta_nunu_fix.informasi (id, judul, tanggal, keterangan, tempat, image) VALUES (1, 'Expo Proyek IV', '2019-08-21', 'Kegiatan expo adalah kegiatan rutin mahasiswa untuk mempresentasikan hasil pembelajaranya selama melaksanakan mata kuliah proyek I', 'Depan Gedung TI', '1566392365_34069048_2065285883541691_2633774061594869760_n.jpg');
INSERT INTO ta_nunu_fix.informasi (id, judul, tanggal, keterangan, tempat, image) VALUES (2, 'Expo Proyek V', '2019-08-21', 'Kegiatan expo adalah kegiatan rutin mahasiswa untuk mempresentasikan hasil pembelajaranya selama melaksanakan mata kuliah proyek V', 'Belakang Direktorat', '1566392391_34048615_1571123122986203_7500382722800484352_n.jpg');
create table kelas
(
    id   int auto_increment
        primary key,
    nama varchar(100) not null
);

INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (1, 'D3TI1A');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (2, 'D3TI1B');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (3, 'D3TI1C');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (4, 'D3TI1D');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (5, 'D3TI2A');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (6, 'D3TI2B');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (7, 'D3TI2C');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (8, 'D3TI2D');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (9, 'D3TI3A');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (10, 'D3TI3B');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (11, 'D3TI3C');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (12, 'D3TI3D');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (13, 'RPL.1');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (14, 'RPL.3');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (15, 'RPL.2A');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (16, 'RPL.2B');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (17, '1A');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (18, '1B');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (19, '1C');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (20, '2A');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (21, '2B');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (22, '2C');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (23, '3A');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (24, '3B');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (25, '3C');
INSERT INTO ta_nunu_fix.kelas (id, nama) VALUES (26, '3D');
create table kelompok
(
    id            int          null,
    nama_kelompok varchar(400) null
);

INSERT INTO ta_nunu_fix.kelompok (id, nama_kelompok) VALUES (1, 'Kelompok 1');
INSERT INTO ta_nunu_fix.kelompok (id, nama_kelompok) VALUES (2, 'Kelompok 2');
INSERT INTO ta_nunu_fix.kelompok (id, nama_kelompok) VALUES (3, 'Kelompok 3');
INSERT INTO ta_nunu_fix.kelompok (id, nama_kelompok) VALUES (4, 'Kelompok 4');
create table logbook
(
    id          int auto_increment
        primary key,
    id_kelas    int          null,
    id_kelompok int          null,
    id_mhs      int          null,
    id_proposal int          not null,
    judul       varchar(100) not null,
    isi         varchar(100) not null,
    keterangan  varchar(100) null,
    time        date         not null
);

INSERT INTO ta_nunu_fix.logbook (id, id_kelas, id_kelompok, id_semester_mhs, id_proposal, judul, isi, keterangan, time) VALUES (1, 10, 2, 4, 3, 'Test', 'testq', 'tset', '2019-09-17');
create table mahasiswa
(
    id      int auto_increment
        primary key,
    nim     varchar(10)        null,
    nama    varchar(100)       not null,
    tahun   char(4) default '' not null,
    user_id int                null
);

INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (1, '1603030', 'ambar', '2019', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (2, '1603030', 'ijang', '2019', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (3, '1603030', 'amel', '2019', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (4, '1603030', 'samsul', '2019', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (5, '160', 'adehilmi', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (6, '160', 'uciramadhani', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (7, '160', 'fahmi muhammad', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (8, '160', 'aditya', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (9, '1603053', 'dhandi', '2018', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (10, '16030', 'rizkhan', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (11, '16030', 'nezla', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (12, '16030', 'atun', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (13, '16030', 'atun', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (14, '16030', 'atun', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (15, '16030', 'atun', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (16, '16030', 'atun', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (17, '16030', 'atun', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (18, '16030', 'atun', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (19, '16030', 'neta', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (20, '16030', 'neta', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (21, '16030', 'rifaldi', '', null);
INSERT INTO ta_nunu_fix.mahasiswa (id, nim, nama, tahun, user_id) VALUES (286, '1231', 'fasfasfas', '2019', 327);
create table pengampu
(
    id             int auto_increment
        primary key,
    nik            varchar(100) not null,
    nama           varchar(100) not null,
    is_coordinator tinyint(1)   null,
    user_id        int          null
);

INSERT INTO ta_nunu_fix.pengampu (id, nik, nama, is_coordinator, user_id) VALUES (1, '16039001', 'lukman sifa', null, null);
INSERT INTO ta_nunu_fix.pengampu (id, nik, nama, is_coordinator, user_id) VALUES (2, '16039002', 'munengsih sari bunga', null, null);
INSERT INTO ta_nunu_fix.pengampu (id, nik, nama, is_coordinator, user_id) VALUES (3, '1241241', 'tagqerc', null, 328);
INSERT INTO ta_nunu_fix.pengampu (id, nik, nama, is_coordinator, user_id) VALUES (4, '1241241', 'tagqerc', 1, 329);
INSERT INTO ta_nunu_fix.pengampu (id, nik, nama, is_coordinator, user_id) VALUES (5, '1241241', 'tagqerc', 1, 330);
create table proposal
(
    id              int auto_increment
        primary key,
    id_mhs          int           not null,
    judul           varchar(100)  not null,
    file            varchar(100)  not null,
    status          int default 0 not null,
    time            date          not null,
    declined_reason varchar(500)  null
);

INSERT INTO ta_nunu_fix.proposal (id, id_semester_mhs, judul, file, status, time, declined_reason) VALUES (1, 2, 'Proposal Rizaldi', '1567065791_TI KJM Ganjil 2018-2019.xlsx', 1, '2019-01-01', null);
INSERT INTO ta_nunu_fix.proposal (id, id_semester_mhs, judul, file, status, time, declined_reason) VALUES (2, 2, 'Proposal Rizaldi 2', '1567065911_datadosen.xlsx', 1, '2019-01-01', null);
INSERT INTO ta_nunu_fix.proposal (id, id_semester_mhs, judul, file, status, time, declined_reason) VALUES (3, 4, 'Proposal Samsul', '1567065949_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01', null);
INSERT INTO ta_nunu_fix.proposal (id, id_semester_mhs, judul, file, status, time, declined_reason) VALUES (4, 1, 'Proposal Ambar', '1567066076_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01', null);
INSERT INTO ta_nunu_fix.proposal (id, id_semester_mhs, judul, file, status, time, declined_reason) VALUES (5, 8, 'Proposal Aditya', '1567066272_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01', null);
INSERT INTO ta_nunu_fix.proposal (id, id_semester_mhs, judul, file, status, time, declined_reason) VALUES (6, 8, 'Proposal Aditya 2', '1567066520_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01', null);
INSERT INTO ta_nunu_fix.proposal (id, id_semester_mhs, judul, file, status, time, declined_reason) VALUES (7, 6, 'Proposal Uci', '1567066784_ANGKET PENILAIAN - KUISIONER.docx', 1, '2019-01-01', null);
INSERT INTO ta_nunu_fix.proposal (id, id_semester_mhs, judul, file, status, time, declined_reason) VALUES (8, 1, 'Proposal Pengajuan Dana', '1568617356_TI KJM Ganjil 2018-2019.xlsx', 1, '2019-09-16', null);
INSERT INTO ta_nunu_fix.proposal (id, id_semester_mhs, judul, file, status, time, declined_reason) VALUES (9, 1, 'Proposal Kelompok 1', '1568637138_KOMUNIKASI BISNIS.docx', 2, '2019-09-16', 'Test');
create table revisi
(
    id        int auto_increment
        primary key,
    judul     varchar(100) not null,
    id_mhs    int          not null,
    file      varchar(100) not null,
    deskripsi varchar(100) not null
);


create table semester_calender
(
    id       int          not null
        primary key,
    tahun    char(16)     null,
    semester varchar(400) null,
    dari_tgl date         null,
    ke_tgl   date         null
);

INSERT INTO ta_nunu_fix.semester_calender (id, tahun, semester, dari_tgl, ke_tgl) VALUES (1, '2019', 'Semester Genap', '2019-07-01', '2019-10-31');
INSERT INTO ta_nunu_fix.semester_calender (id, tahun, semester, dari_tgl, ke_tgl) VALUES (2, '2019', 'Semester Ganjil', '2018-11-01', '2019-02-28');
create table semester_mahasiswa
(
    id                   int auto_increment
        primary key,
    semester_calender_id int null,
    kelompok_id          int null,
    mahasiswa_id         int not null,
    kelas_id             int null
);

INSERT INTO ta_nunu_fix.semester_mahasiswa (id, semester_calender_id, kelompok_id, mahasiswa_id, kelas_id) VALUES (1, 1, 1, 286, 2);
create table semester_pengampu
(
    id                   int auto_increment
        primary key,
    kelas_id             int null,
    semester_kalender_id int null,
    pengampu_id          int null
);

INSERT INTO ta_nunu_fix.semester_pengampu (id, kelas_id, semester_kalender_id, pengampu_id) VALUES (1, 5, 1, 5);
create table user
(
    id           int auto_increment
        primary key,
    username     varchar(100) not null,
    password     varchar(100) not null,
    id_user_role int          null,
    status       int          not null,
    token        varchar(100) not null
);

INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (1, 'nugie', '$2y$13$MWTQljb2XlBTZI3OPx0NzOSfP4FGakwgjIrxtRTO02RlMwe1wFuKu', 1, 1, '9E4V9_8F6vqzq-fMP0aFCxpygESXqbJ72pJHJC2CpE6xsyIrJvfzLw-0LWiipFnsE83Tx-KBKP6JunEfciYx6ZtFRuEmt0jlNSYe');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (2, 'sams', '$2y$13$MWTQljb2XlBTZI3OPx0NzOSfP4FGakwgjIrxtRTO02RlMwe1wFuKu', 3, 1, '9E4V9_8F6vqzq-fMP0aFCxpygESXqbJ72pJHJC2CpE6xsyIrJvfzLw-0LWiipFnsE83Tx-KBKP6JunEfciYx6ZtFRuEmt0jlNSYe');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (3, 'ambar', '$2a$10$jiXcH4AnMqLrGUtEBBA3TOJHeWMJN0FI8PuDr39kjMXfwtulEdqk6', 2, 1, '97RT49_vnSynIqDsBbiSAXwfKcRNlyO96reHusm3w2YtoPz8_dunf5GYqX66ba4FMmvABpK86L2hQnPXmKZv5TNDikjN8NLjS2zI');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (4, 'rizaldi', '$2y$13$bxLSMqtLmDwUyVE3Dko7WOo.n6mR606kcN7UhkqOCMLlI1tts7/ei', 2, 1, 'rwx8ff5ihb942dSiNkwK2CUf0cjKM4LfpKTjpklPYUa7QcCndSgBKbD05B6eiZmWZtgpH-90o7pO1wak2bBpVX3kxABzIsRyuf9Y');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (5, 'amelia', '$2y$13$gg5C6HFXU7LevK0QgF1RpuKv6.vZCCqGs9Rn28tT.hEda1f4FGi4G', 2, 1, 'kEwk0Xax8uMgHEdvhO6-FmEGEpLkPU9B-ZjDqezqWobzKvNJzc5_NQ6DGpCnvZ_R1_J3tXle7gEKaC_rQONm_-8C0sXxHEumB9u0');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (6, 'samsul', '$2a$10$jiXcH4AnMqLrGUtEBBA3TOJHeWMJN0FI8PuDr39kjMXfwtulEdqk6', 2, 1, 'vEwZmTA7a-fahY5be5nJKXVsuNc3FmoAucCDkMxPxG_yl49SVKm5tHgvIie_SFantjkgCazwhB6b55sKTpHT-KVwghcHwoMWNZ3D');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (7, 'palukman', '$2a$10$jiXcH4AnMqLrGUtEBBA3TOJHeWMJN0FI8PuDr39kjMXfwtulEdqk6', 4, 1, 'Df6_bcWc-J4WO4cDquERBP57Tz5Y-clXJM7EHMUzl0WDKjBnsZYDzIVnALhGh-Bcz5UpblTuHzSOUXq4Fso6TX4FoyT5hBUfTqMS');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (8, 'adehilmi', '$2y$13$I4lPVaVUdA2xT29opOHitOCml68dFecdv5KwWNpP20Il9gJIGyeyu', 2, 1, 'xBAtmYUKqFnpMSHqASu7SyHTzzBpVupvI8gDPrhlXf80dT8tN0AMpQ2l_A--qeg2-x9H0eflMzNbm01kkKhw3wNBPhkWpiPPo-3H');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (9, 'uci', '$2y$13$4kl5IXCuWMYxcaA/.OzoF.1zdZxeUAWGTfjikgc65Unrj1mItNpF2', 2, 1, 'ljFWrHf86u75utqAROi6rVdThDAkXrkI_4JX7jwnAsrfQH8T5G1r8DQHf46uSg3AQCf_St1mgRio0Na2fFCXZwBlchf2wH4DK3Xw');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (10, 'fahmi', '$2y$13$mJ3AtkP.o90aLPKBrV1KOe4Q5cIMyaSYJEKWS/sgOheGTToQFWVWy', 2, 1, 'krJAVi-FKllJ0SX7rEhx9EzYwNLycRCQ-2uu7PUsgSyMkXKc7VTsj9SwC-QWQqkA6Uq20Ty44fTd1aNdP0CqaSAoMAqXe2hcdgcs');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (11, 'aditya', '$2y$13$2dZYfOWdkYVvjyPDQfBSyudtuCumImRguHnkTKj5FhbHtAkD1mwn6', 2, 1, 'epUyrhwpfJP_YoJV6DFgDjoHOoKdBI1jnijgJo4ZyYVLm4A24qqlyeFhIQDHyIUQgIssjnaqcBOuPrxlIzcY1zclHS6aOQVfGduE');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (12, 'bumun', '$2a$10$jiXcH4AnMqLrGUtEBBA3TOJHeWMJN0FI8PuDr39kjMXfwtulEdqk6', 4, 1, 'epUyrhwpfJP_YoJV6DFgDjoHOoKdBI1jnijgJo4ZyYVLm4A24qqlyeFhIQDHyIUQgIssjnaqcBOuPrxlIzcY1zclHS6aOQVfGduE');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (13, 'dhandi', '$2y$13$R.efyUziN6aVfNjKffxrj.vB9.7B0pUHB5t1zoMLOe03hHdXdNkYe', 2, 1, 'NjzdgVMira-mgct3EAep5uGgPO9p_HL4MH3ioXbR6Og3__X40wzBXVq2dKx88sanf8boN-kELUuJfVLHYD_AjU0nB-wzB_zHVdWg');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (14, 'rizkhan', '$2y$13$gqyfkiSNK2naEzvGyFnRVuNSTMLIMOqkfzvvB39smCjSK.ulR6Giu', 2, 1, 'Q2uCRlIRtfNLxYBtk7AI29dmtoPPUM5DtkQFOACZRjsV0q2OMF3gCLgOYc5hHlz88edp5YqGUUUwRljqhsTU9MNCNrhFvD3QHcrc');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (15, 'nezla', '$2y$13$ohh0RCFSjeGrs1lQSgp7leb20dn0AoGAbjzFnKAetBO3ClgRogCDi', 2, 1, 'yaB5JWYVo72QHz57VUF3ndGj6lRKD_tLHRXin_7peJoZJrOOqOzd9LqNdb7DwrysSLEEhuTxXhdGyTPcmDHnkSVtd-HLITL0b_iR');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (16, 'atun', '$2y$13$ayQ5ZThGciF71bcp3Jmoku/AZCqYN/7rxADx3AzbiGZAsY2.ILjNy', 2, 1, 'fyGiBmKtNCjcOogutn_hKsPQbv41Vd8WaQvpd7VLhA5pzyeuUIkyH9c2zwxTLaUvG7oLaDj_FRJuWmaGduHBxqhK37lRNlcY-Ud-');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (17, 'neta', '$2y$13$15ls8Yi8OFTssBWi9GjCGOTvSWoeKsJTxrMQV6FfVT8EHswu6Eus6', 2, 1, 'mePhR2bsoWDYcpZk3DnWy56H7h9uRTzulmeD3TI2ZoznBw9RQftp562grHCeS0iIy1ItcvYchLCH8dZInXFlrCJ-Ehe0woKQLojU');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (18, 'rifal', '$2y$13$zKssfxjoXW4K96SwUuQ1FuqbL6M7XhRD2NaaT3rQ83soodxFbJEhK', 2, 1, 'iSuQHtiwomvyjsdoIUmcZViOcJgfCUFnGnsmHrUIbX_powRb13uK0Ox2mVTRus7vSpw0f3VHBDE8D2JUrBERNiOxML07NKPypzpc');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (19, 'kordinator1', '$2a$10$jiXcH4AnMqLrGUtEBBA3TOJHeWMJN0FI8PuDr39kjMXfwtulEdqk6', 3, 1, 'F-HgeQEssDhPrfFo7xcrxd4K2mm14W-jSeq6OfKlnJM6O4JD-D-0CzxlbJ71cIZ4cBYc-ifD8ZakxApwsWWNe_In9MVK-Mudbr3x');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (20, 'kordinator2', '$2a$10$jiXcH4AnMqLrGUtEBBA3TOJHeWMJN0FI8PuDr39kjMXfwtulEdqk6', 3, 1, 'FfYMVCY3kkoHsHoVE6IOZFrx5Y9Oc4hCE7G2WoX9GKHNToiKWP4o-vVlDwa0nMvb4fdMKoYaa1qD4FgoP_OXA72lU07sYQAqadh3');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (21, 'bluespy', '$2a$10$jiXcH4AnMqLrGUtEBBA3TOJHeWMJN0FI8PuDr39kjMXfwtulEdqk6', 1, 1, '9E4V9_8F6vqzq-fMP0aFCxpygESXqbJ72pJHJC2CpE6xsyIrJvfzLw-0LWiipFnsE83Tx-KBKP6JunEfciYx6ZtFRuEmt0jlNSYe');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (324, 'testts', '$2y$13$HWLDvpGx0kvozX7tRCdfhexJzXJNqa/6m6tCooXCw.9GXIEWd.K8W', 2, 1, 'jxQ-4uLcxGgY5xGTllifBEF7aWoco_r9d01nTISEzx-oDkOYrpr8W8WBt3Ay5uaNlF6_jcu5wyRYJMdlkFRHFKHwOJ5qwkGmu9nl');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (325, 'testts12', '$2y$13$22suu.Ign3MfldDSCzduheZI7.zvVK4mxEFNrOvBIPJYoudoUdunW', 2, 1, 'SPXUAEd9j2J1KfbBR37HhX6n4ztd8rZneHnsgQAN5V-RHxkhji1sHZjygEU8sexTddLHDp42HQnvlLkp9pqQQjkn-w19gvTuQku5');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (326, 'testts124', '$2y$13$gT/YtxvN5h0kTonxWB8.B.dqgItTU4kONy9QizUB7Wb7HKv7XrXym', 2, 1, 'fl6XdmSAPZrTlhVGXWbS9Sa_qVvCSfVJUF4Heu_PkgZRZcC4BvBgFZb3Zc1K295_W62x6uSdzUUJ1VzsntfYNyS4yKWOg97yY7Kc');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (327, 'afasfas', '$2y$13$f1ZF49HqC1S12GN5FQKyXeG.D.0KNBk/S8adoEwoTAt5o2OS3vtV.', 2, 1, 'bsq7HSgcmRfDuRRuAT_ylLcgWVOyQHR__nbt8TVrJ0BiijQ9iRgAW-ziE1Cj1JUjTOprCi6rrV48GF91qdrLmI8R0oNH5MV8yuX7');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (328, 'tsettset', '$2y$13$oDsbuFpiauQopvfItL8aSu0YyeJQsQZnomAFM4i.9TG6GM5BxqXUa', 3, 1, '0Nzq3Bn8YlNM17wWSWu2XfkO98G-g4MUBxk6r9MTHNtkeu5MSpFaOILwEk0t-_15lRaFiAHKhlP14Ko-Wm2tc9iY-LHlzwPmNrtU');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (329, 'tsettsetgg', '$2y$13$0jcInMecYY/K39Xfkbmw7OS2hUe4n2bIZe0iH5v8QPV7.Ehw4/Axy', 3, 1, 'kZYZQuEwCxB_yrrUTLAK8ZgJIwju8cIfoGJhh0qUjFUMvvTHFvgjrHE3OFnnw26W0TsSCJNNsq--w6bocg1a8lZV5jH_37QKErZr');
INSERT INTO ta_nunu_fix.user (id, username, password, id_user_role, status, token) VALUES (330, 'tsettsetggsf', '$2y$13$s105cT/xpC03E1PBHKR8Ce89dPi0/J5HIxLBJnO7UQG88NH5uj82O', 3, 1, 'QeOaHh96ml1iMQEDaCP07gIrqnWmtaTmup-VSj8D5VkrYEazdxHZ8nC-ZR9C1ZAZ3IjLoSCwbz9dv7xEr3C7C_YPRe0twKMnJ82l');
create table user_role
(
    id   int auto_increment
        primary key,
    nama varchar(100) not null
);

INSERT INTO ta_nunu_fix.user_role (id, nama) VALUES (1, 'admin');
INSERT INTO ta_nunu_fix.user_role (id, nama) VALUES (2, 'mahasiswa');
INSERT INTO ta_nunu_fix.user_role (id, nama) VALUES (3, 'pengampu');