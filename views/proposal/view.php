<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Proposal */

$this->title = $model->judul;
$this->params['breadcrumbs'][] = ['label' => 'Proposal', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="proposal-view box box-primary">
    <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            
            'attributes' => [
                // 'id',
                // 'id_mhs',
                // 'id_kelompok',
                // 'id_kelas',
                // 'judul',
                // 'file',
                [  
                    'attribute' => 'id_mhs',
                    'value' => function($data)
                    {
                            // Cara pemanggilan 1 yang ada di model buku.
                            //return $data->getPenulis();

                            // Cara pemanggilan 2 yang ada di model buku.
                        return $data->mahasiswa->nama;
                    }
                ],
                [  
                    'attribute' => 'id_kelas',
                    'value' => function($data)
                    {
                            // Cara pemanggilan 1 yang ada di model buku.
                            //return $data->getPenulis();

                            // Cara pemanggilan 2 yang ada di model buku.
                        return $data->kelas->nama;
                    }
                ],
                [  
                    'attribute' => 'id_kelompok',
                    'value' => function($data)
                    {
                            // Cara pemanggilan 1 yang ada di model buku.
                            //return $data->getPenulis();

                            // Cara pemanggilan 2 yang ada di model buku.
                        return @$data->kelompok->nama_kelompok;
                    }
                ],
                'judul',
                [
                    'attribute' => 'file',
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'text-align:center; width:80px'],
                    'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                    'value' => function ($model)
                    {
                        if ($model->file !== '') {
                            return '<a href="' . Yii::$app->request->baseUrl.'/upload/' . $model->file .'"><div align="center"><button class="btn btn-success glyphicon glyphicon-download-alt" type="submit"></button></div></a>';
                        }
                        else{
                            return '<div align="center"><h1>File tidak ada</h1></div>';
                        }

                    },
                ],
            ],
        ]) ?>
    </div>
</div>
