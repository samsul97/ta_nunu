<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\file\FileInput;
use app\models\Kelompok;
use app\models\Kelas;
use app\models\Proposal;

/* @var $this yii\web\View */
/* @var $model app\models\Revisi */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if (Yii::$app->user->identity->id_user_role == 1): ?>
<div class="revisi-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'id_proposal')->widget(Select2::classname(),
            [
                'data' => Proposal::getList(),
                'options' => [
                    'placeholder' => '-Pilih Proposal-',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],

            ]);
        ?>
        <?= $form->field($model, 'file_upload')->widget(FileInput::classname(),
            [
                'options' => ['multiple' => false],
            ]);
        ?>
        <?= $form->field($model, 'deskripsi')->textArea(['maxlength' => true]) ?>
    </div>
    <div class="box-footer">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php endif ?>

<?php if (Yii::$app->user->identity->id_user_role == 2): ?>
<div class="revisi-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'id_proposal')->widget(Select2::classname(),
            [
                'data' => Proposal::getList(),
                'options' => [
                    'placeholder' => '-Pilih Proposal-',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],

            ]);
        ?>
        <?= $form->field($model, 'file_upload')->widget(FileInput::classname(),
            [
                'options' => ['multiple' => false],
            ]);
        ?>
        <?= $form->field($model, 'deskripsi')->textArea(['maxlength' => true]) ?>
    </div>
    <div class="box-footer">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php endif ?>

<?php if (Yii::$app->user->identity->id_user_role == 3): ?>
<div class="revisi-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'id_proposal')->widget(Select2::classname(),
            [
                'data' => Proposal::getList(),
                'options' => [
                    'placeholder' => '-Pilih Proposal-',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],

            ]);
        ?>
        <?= $form->field($model, 'file_upload')->widget(FileInput::classname(),
            [
                'options' => ['multiple' => false],
            ]);
        ?>
        <?= $form->field($model, 'deskripsi')->textArea(['maxlength' => true]) ?>
    </div>
    <div class="box-footer">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php endif ?>