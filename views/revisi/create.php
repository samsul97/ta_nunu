<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Revisi */

$this->title = 'Create Revisi';
$this->params['breadcrumbs'][] = ['label' => 'Revisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="revisi-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
