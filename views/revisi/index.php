<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Proposal;
use app\models\Kelompok;
use app\models\Kelas;
use app\models\Mahasiswa;
/* @var $this yii\web\View */
/* @var $searchModel app\models\RevisiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Revisi';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if (Yii::$app ->user->identity->id_user_role==1):?>
    <div class="revisi-index box box-primary">
        <!-- <div class="box-header with-border">
            <?= Html::a('Create Revisi', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
        </div> -->
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],
               // 'id',
                    'judul',
                // 'id_kelompok',
                // 'id_proposal',
                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],

                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelas->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_proposal',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Proposal::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->proposal->judul;
                        }
                    ],
                    [
                        'attribute' => 'file',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align:center; width:80px'],
                        'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                        'value' => function ($model)
                        {
                            if ($model->file !== '') {
                                return '<a href="' . Yii::$app->request->baseUrl.'/upload/' . $model->file .'"><div align="center"><button class="btn btn-success glyphicon glyphicon-download-alt" type="submit"></button></div></a>';
                            }
                            else{
                                return '<div align="center"><h1>File tidak ada</h1></div>';
                            }

                        },
                    ],
                    'deskripsi',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (Yii::$app ->user->identity->id_user_role==2):?>
    <div class="revisi-index box box-primary">
        <div class="box-header with-border">
            <?= Html::a('Create Revisi', ['create'], ['class' => 'btn btn-info btn-flat']) ?>
        </div>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],
               // 'id',
                    'judul',
                // 'id_kelompok',
                // 'id_proposal',
                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],

                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_proposal',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Proposal::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->proposal->judul;
                        }
                    ],
                    [
                        'attribute' => 'file',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align:center; width:80px'],
                        'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                        'value' => function ($model)
                        {
                            if ($model->file !== '') {
                                return '<a href="' . Yii::$app->request->baseUrl.'/upload/' . $model->file .'"><div align="center"><button class="btn btn-success glyphicon glyphicon-download-alt" type="submit"></button></div></a>';
                            }
                            else{
                                return '<div align="center"><h1>File tidak ada</h1></div>';
                            }

                        },
                    ],
                    'deskripsi',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (Yii::$app ->user->identity->id_user_role==3):?>
    <div class="revisi-index box box-primary">
        <div class="box-header with-border">
        </div>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],
               // 'id',
                    'judul',
                // 'id_kelompok',
                // 'id_proposal',
                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],
                    [
                        'attribute' => 'id_proposal',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Proposal::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->proposal->judul;
                        }
                    ],
                    [
                        'attribute' => 'file',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align:center; width:80px'],
                        'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                        'value' => function ($model)
                        {
                            if ($model->file !== '') {
                                return '<a href="' . Yii::$app->request->baseUrl.'/upload/' . $model->file .'"><div align="center"><button class="btn btn-success glyphicon glyphicon-download-alt" type="submit"></button></div></a>';
                            }
                            else{
                                return '<div align="center"><h1>File tidak ada</h1></div>';
                            }

                        },
                    ],
                    'deskripsi',

                    [
                        'template' => '{view}',
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
<?php endif ?>

<?php if (Yii::$app ->user->identity->id_user_role==4):?>
    <div class="revisi-index box box-primary">
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    [
                        'class' => 'yii\grid\SerialColumn',
                        'header' => 'No',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'contentOptions' => ['style' => 'text-align:center']
                    ],

                    [
                        'attribute' => 'id_mhs',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Mahasiswa::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->mahasiswa->nama;
                        }
                    ],
                    'judul',
                    [
                        'attribute' => 'id_kelompok',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelompok::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelompok->nama_kelompok;
                        }
                    ],

                    [
                        'attribute' => 'id_kelas',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Kelas::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->kelas->nama;
                        }
                    ],
                    
                    [
                        'attribute' => 'id_proposal',
                        'format' => 'raw',
                        'headerOptions' => ['style' => 'text-align:center'],
                        'filter' => Proposal::getList(),
                        'contentOptions' => ['style' =>'text-align:center;'],
                        'value' => function ($data) {
                            return @$data->proposal->judul;
                        }
                    ],
                    [
                        'attribute' => 'file',
                        'format' => 'raw',
                        'contentOptions' => ['style' => 'text-align:center; width:80px'],
                        'headerOptions' => ['style' => 'text-align:center', 'width' => '20'],
                        'value' => function ($model)
                        {
                            if ($model->file !== '') {
                                return '<a href="' . Yii::$app->request->baseUrl.'/upload/' . $model->file .'"><div align="center"><button class="btn btn-success glyphicon glyphicon-download-alt" type="submit"></button></div></a>';
                            }
                            else{
                                return '<div align="center"><h1>File tidak ada</h1></div>';
                            }

                        },
                    ],
                    'deskripsi',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'contentOptions' => ['style' => 'text-align:center;width:80px']
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?php endif ?>