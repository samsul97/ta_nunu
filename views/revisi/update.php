<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Revisi */

$this->title = 'Update Revisi: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Revisis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="revisi-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
