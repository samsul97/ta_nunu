<?php

use app\models\Kelas;
use app\models\Kelompok;
use app\models\Mahasiswa;
use app\models\SemesterCalender;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SemesterMahasiswa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="semester-mahasiswa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'semester_kalender_id')->widget(Select2::class,
        [
            'data' => SemesterCalender::getList(),
            'options' => [
                'placeholder' => '-Pilih Semester-',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
    ?>

    <?= $form->field($model, 'kelas_id')->widget(Select2::class,
        [
            'data' => Kelas::getList(),
            'options' => [
                'placeholder' => '-Pilih Kelas-',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
    ?>

    <?= $form->field($model, 'mahasiswa_id')->widget(Select2::class,
        [
            'data' => Mahasiswa::getList(),
            'options' => [
                'placeholder' => '-Pilih Mahasiswa-',
            ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])
    ?>

    <?= $form->field($model, 'kelompok_id')->label(false)->hiddenInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
