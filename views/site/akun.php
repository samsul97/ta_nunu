<?php

use app\models\CreateUser;
use app\models\Kelas;
use app\models\Kelompok;
use app\models\MJurusan;
use app\models\SemesterCalender;
use app\models\UserRole;
use kartik\select2\Select2;
use shaqman\widgets\inlinescript\InlineScript;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

// use app\models\Mjurusan;
// use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
$this->title = 'Create Account';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
$fieldOptions3 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions4 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-phone form-control-feedback'></span>"
];

$fieldOptions5 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-user form-control-feedback'></span>"
];

$fieldOptions6 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-home form-control-feedback'></span>"
];
?>

<div class="login-box">
    <div class="login-box-body">
        <div style="text-align: center;"><p class="login-box-msg">Create Account</p></div>

        <?php $form = ActiveForm::begin(['id' => 'TambahAkun', 'enableClientValidation' => false]); ?>

        <?= $form->field($model, 'semester_calender_id')->widget(Select2::class,
            [
                'data' => SemesterCalender::getList(),
                'options' => [
                    'placeholder' => '-Pilih Semester-',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
        ?>

        <?= $form->field($model, 'id_user_role', ['inputOptions'=>['id'=>'role-select2']])->widget(Select2::class,
            [
                'data' => UserRole::getList(),
                'options' => [
                    'placeholder' => '-Pilih Pengguna-',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
        ?>

        <?php InlineScript::begin(); ?>
        <script>
            $('#role-select2').on('change', function (e) {
                let data = $('#role-select2').select2('data');
                $("#kelompok_container_id").hide();
                $("#class_container_id").hide();
                $("#coordinator_container_id").hide();
                if(data[0].id === '2') {
                    $("#class_container_id").show();
                    $("#kelompok_container_id").show();
                    $("#identity_number_container").show()
                } else if(data[0].id === '3') {
                    $("#class_container_id").show();
                    $("#coordinator_container_id").show();
                    $("#identity_number_container").show()
                }
            });
        </script>

        <?php InlineScript::end(); ?>

        <div id="class_container_id" style="display: none">
        <?= $form->field($model, 'id_kelas')->widget(Select2::class,
            [
                'data' => Kelas::getList(),
                'options' => [
                    'placeholder' => '-Pilih Kelas-',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
        ?>
        </div>

        <div id="kelompok_container_id"  style="display: none">
        <?= $form->field($model, 'id_kelompok')->widget(Select2::class,
            [
                'data' => Kelompok::getList(),
                'options' => [
                    'placeholder' => '-Pilih Kelompok-',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])
        ?>
        </div>

        <div id="coordinator_container_id"  style="display: none">
        <?= $form
            ->field($model, 'is_coordinator')
            ->checkbox() ?>
        </div>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => 'username']) ?>

        <div id="identity_number_container"  style="display: none">
        <?= $form
            ->field($model, 'identity_number', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => 'nim/nik']) ?>
        </div>

        <?= $form
            ->field($model, 'nama', $fieldOptions5)
            ->label(false)
            ->textInput(['placeholder' => 'nama']) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => 'password']) ?>


        <div class="row">
            <div class="col-xs-12">
                <?= Html::submitButton('Create Account', ['class' => 'btn btn-block btn-sosial btn-success btn-flat', 'name' => 'login-button']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>