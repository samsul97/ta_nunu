<?php

use app\models\Pengampu;
use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Mahasiswa;
use app\models\Logbook;
use app\models\Proposal;
use app\models\Kordinator;
use app\models\Kelompok;
use app\models\Informasi;
use app\models\Revisi;
use app\models\Kelas;
use app\models\Semester;
use miloschuman\highcharts\Highcharts;
use yii\widgets\LinkPager;

$this->title = 'Selamat datang di Aplikasi Mata Kuliah Proyek';
?>

<?php if (Yii::$app->user->identity->id_user_role == 1): ?>
  <div class="site-index">
    <br>
    <br>
    <div class="row" style="margin: 3px;">
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <p>Mahasiswa</p>
            <h3><?= Yii::$app->formatter->asInteger(Mahasiswa::getCount()); ?></h3>
          </div>
          <div class="icon">
            <i class="fa fa-user-o"></i>
          </div>
          <a href="<?=Url::to(['mahasiswa/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-blue">
          <div class="inner">
            <p>logbook</p>

            <h3><?= Yii::$app->formatter->asInteger(Logbook::getCount()); ?></h3>
          </div>
          <div class="icon">
            <i class="fa fa-user-o"></i>
          </div>
          <a href="<?=Url::to(['logbook/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-gray">
          <div class="inner">
            <p>kordinator</p>

            <h3><?= Yii::$app->formatter->asInteger(Pengampu::getCoordinatorCount()) ?></h3>
          </div>
          <div class="icon">
            <i class="fa fa-wrench"></i>
          </div>
          <a href="<?=Url::to(['kordinator/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-white">
          <div class="inner">
            <p>Kelompok</p>

            <h3><?= Yii::$app->formatter->asInteger(Kelompok::getCount()); ?></h3>
          </div>
          <div class="icon">
            <i class="fa fa-calendar"></i>
          </div>
          <a href="<?=Url::to(['kelompok/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>

    <div class="row" style="margin: 3px;">
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-blue">
          <div class="inner">
            <h3><?= Yii::$app->formatter->asInteger(Revisi::getCount()); ?></h3>
            <p>revisi</p>
          </div>
          <div class="icon">
            <i class="fa fa-globe"></i>
          </div>
          <a href="<?=Url::to(['revisi/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">

        <div class="small-box bg-green">
          <div class="inner">
            <h3><?= Yii::$app->formatter->asInteger(Proposal::getCount()); ?><sup style="font-size: 20px"></sup></h3>
            <p>Proposal</p>
          </div>
          <div class="icon">
            <i class="fa fa-user-o"></i>
          </div>
          <a href="<?=Url::to(['proposal/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">

        <div class="small-box bg-orange">
          <div class="inner">
            <h3><?= Yii::$app->formatter->asInteger(Kelas::getCount()); ?></h3>
            <p>kelas</p>
          </div>
          <div class="icon">
            <i class="fa fa-user-o"></i>
          </div>
          <a href="<?=Url::to(['kelas/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">

        <div class="small-box bg-red">
          <div class="inner">
            <h3><?= Yii::$app->formatter->asInteger(Informasi::getCount()); ?></h3>
            <p>Informasi</p>
          </div>
          <div class="icon">
            <i class="fa fa-graduation-cap"></i>
          </div>
          <a href="<?=Url::to(['informasi/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
<?php endif ?>



<?php if (Yii::$app->user->identity->id_user_role == 2): ?>
  <div class="row">
  <?php foreach ($provider->getModels() as $informasi) {?> 
    <!-- Kolom box mulai -->
    <div class="col-md-4">
      <!-- Box mulai -->
      <div class="box box-widget">
        <div class="box-header with-border">
          <div class="user-block">
            <span class="username"><?= Html::a($informasi->judul, ['informasi/view', 'id' => $informasi->id]); ?></span>
            <span class="description"> Tanggal Event :  <?= $informasi->tanggal; ?></span>
          </div>
          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Mark as read"><i class="fa fa-circle-o"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <img class="img-responsive pad" style="height: 300px;" src="<?= Yii::$app->request->baseUrl.'/upload/'.$informasi['image']; ?>" alt="Photo">
          <p>Tempat : <?= $informasi->tempat; ?></p>
          <p>Keterangan : <?= substr($informasi->keterangan,0,40);?> ...</p>
          <?= Html::a("<i class='fa fa-eye'> Detail informasi</i>",["informasi/view","id"=>$informasi->id],['class' => 'btn btn-default']) ?>
          <!-- <span class="pull-right text-muted">127 Peminjam - 3 Komentar</span> -->
        </div>
      </div>
      <!-- Box selesai -->
    </div>
    <!-- Kolom box selesai -->  
    <?php
  }
  ?>
</div>

<div class="row">
  <center>
    <?= LinkPager::widget([
      'pagination' => $provider->pagination,
    ]); ?>
  </center>
</div>
  <div class="site-index">
    <br>
    <br>
    <div class="row" style="margin: 3px;">
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <p>Proposal</p>
            <h3><?= Yii::$app->formatter->asInteger(Proposal::getCount()); ?></h3>
          </div>
          <div class="icon">
            <i class="fa fa-user-o"></i>
          </div>
          <a href="<?=Url::to(['proposal/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-blue">
          <div class="inner">
            <p>Logbook</p>

            <h3><?= Yii::$app->formatter->asInteger(Logbook::getCount()); ?></h3>
          </div>
          <div class="icon">
            <i class="fa fa-user-o"></i>
          </div>
          <a href="<?=Url::to(['logbook/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-gray">
          <div class="inner">
            <p>Revisi</p>
            <h3><?= Yii::$app->formatter->asInteger(Revisi::getCount()); ?></h3>
          </div>
          <div class="icon">
            <i class="fa fa-wrench"></i>
          </div>
          <a href="<?=Url::to(['revisi/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-white">
          <div class="inner">
            <p>Kelompok</p>

            <h3><?= Yii::$app->formatter->asInteger(Kelompok::getCount()); ?></h3>
          </div>
          <div class="icon">
            <i class="fa fa-calendar"></i>
          </div>
          <a href="<?=Url::to(['kelompok/index']);?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>     
  </div>
</div>
<?php endif ?>


