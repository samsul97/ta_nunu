<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<?php if (Yii::$app->user->identity->id_user_role == 1): ?>
<div class="user-form">
    <?php
        if ($model->isNewRecord) {
    ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['minlength' => 6, 'maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput(['minlength' => 6, 'maxlength' => true]) ?>

    <div class="form-group">
        <button type="button" class="btn btn-default" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
        <?= Html::submitButton('Simpan', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php
        } else {
    ?>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <button type="button" class="btn btn-default" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
        <?= Html::submitButton('Simpan Username', ['class' => 'btn btn-success']) ?>
        <?= Html::a('<i class="fa fa-key"> Ganti Password</i>', ['change-password', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
    </div>
    <?php ActiveForm::end(); ?>
    <?php   
        }
    ?>
</div>
<?php endif ?>

<!-- From mahasiswa -->
<?php if (Yii::$app->user->identity->id_user_role == 2): ?>
<div class="user-form">
    <div class="form-group">
        <?= Html::a('<i class="fa fa-user"> Biodata</i>', ['mahasiswa/view', 'id' => $model->id_mahasiswa], ['class' => 'btn btn-primary']); ?>
        <?= Html::a('<i class="fa fa-key"> Ganti Password</i>', ['change-password', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <div class="form-group">
        <button type="button" class="btn btn-default" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
        <?= Html::submitButton('Simpan Username', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php endif ?>

<!-- From kordinator -->
<?php if (Yii::$app->user->identity->id_user_role == 3): ?>
<div class="user-form">
    <div class="form-group">
        <?= Html::a('<i class="fa fa-user"> Biodata</i>', ['kordinator/view', 'id' => $model->id_petugas], ['class' => 'btn btn-primary']); ?>
        <?= Html::a('<i class="fa fa-key"> Ganti Password</i>', ['change-password', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <button type="button" class="btn btn-default" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
        <?= Html::submitButton('Simpan Username', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php endif ?>