<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title =  "User : " . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- View admin -->
<?php if (Yii::$app->user->identity->id_user_role == 1): ?>
<div class="user-view box box-primary">
    <div class="box-header">
        <h3 class="box-title">Detail User : <?= $model->username; ?>.</h3>
    </div>
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
            ],
        ]) ?>
        <p>
            <?= Html::a('<i class="fa fa-pencil"> Edit</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
    </div>
</div>
<div class="form-group">
    <button type="button" class="btn btn-default" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
</div>
<?php endif ?>

<!-- View mahasiswa -->
<?php if (Yii::$app->user->identity->id_user_role == 2): ?>
<?php $this->title = $model->username; ?>
<div class="user-view box box-primary">
    <div class="box-header">
        <h3 class="box-title">Profile Mahasiswa : <?= $model->username; ?>.</h3>
    </div>
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
                [
                    'attribute' => 'nim',
                    'value' => function($data) {
                        return $data->mahasiswa->nim;
                    }
                ],
                [
                    'attribute' => 'nama',
                    'value' => function($data) {
                        return $data->mahasiswa->nama;
                    }
                ],
                [
                    'attribute' => 'id_semester',
                    'value' => function($data) {
                        return $data->mahasiswa->id_semester;
                    }
                ],
                [
                    'attribute' => 'tahun',
                    'value' => function($data) {
                        return $data->mahasiswa->tahun;
                    }
                ],
            ],
        ]) ?>
        <p>
            <?= Html::a('<i class="fa fa-pencil"> Edit</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
    </div>
</div>
<div class="form-group">
    <button type="button" class="btn btn-default" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
</div>
<?php endif ?>

<!-- View kordinator -->
<?php if (Yii::$app->user->identity->id_user_role == 3): ?>
<?php $this->title = $model->username; ?>
<div class="user-view box box-primary">
    <div class="box-header">
        <h3 class="box-title">Profile Kordinator : <?= $model->username; ?>.</h3>
    </div>
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
                [
                    'attribute' => 'nama',
                    'value' => function($data) {
                        return $data->kordinator->nik;
                    }
                ],
                [
                    'attribute' => 'alamat',
                    'value' => function($data) {
                        return $data->kordinator->nama;
                    }
                ],
            ],
        ]) ?>
        <p>
            <?= Html::a('<i class="fa fa-pencil"> Edit</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
    </div>
</div>
<div class="form-group">
    <button type="button" class="btn btn-default" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
</div>
<?php endif ?>

<!-- View pengampu -->
<?php if (User::isKordinator()): ?>
<?php $this->title = $model->username; ?>
<div class="user-view box box-primary">
    <div class="box-header">
        <h3 class="box-title">Profile Kordinator : <?= $model->username; ?>.</h3>
    </div>
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
                [
                    'attribute' => 'nama',
                    'value' => function($data) {
                        return $data->kordinator->nik;
                    }
                ],
                [
                    'attribute' => 'alamat',
                    'value' => function($data) {
                        return $data->kordinator->nama;
                    }
                ],
            ],
        ]) ?>
        <p>
            <?= Html::a('<i class="fa fa-pencil"> Edit</i>', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </p>
    </div>
</div>
<div class="form-group">
    <button type="button" class="btn btn-default" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</button>
</div>
<?php endif ?>